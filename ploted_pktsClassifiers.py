'''
    comparison between different classifiers, data based on pkts
'''
print(__doc__)
import sys

import matplotlib.pyplot as plt
import time
import numpy as np
import pandas as pd
import gc
from sklearn.metrics import auc, confusion_matrix, f1_score, roc_curve
from sklearn.preprocessing import label_binarize
import csv
from itertools import cycle

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

import summaryFunc as sFunc
import synthesizedPlot as sPlot
# from . import summaryFunc as sFunc
# from . import synthesizedPlot as sPlot

# clean RAM
gc.collect()

if __name__ == "__main__":

    #loadingPath = "/home/student/Documents/aa_scripter/test4.pcap"
    loadingPath = "/home/student/Documents/aa_scripter/test5.pcap"
    #loadingPath = "/home/student/Documents/aa_scripter/test5_train.pcap"
    #loadingPath = "/home/student/Documents/aa_scripter/test5_train_1.pcap"
    #loadingPath = "/home/student/Documents/aa_scripter/test6.pcap"
    #loadingPath = "/home/student/Documents/aa_scripter/test9.pcap"

    #loadingPath = "/home/student/Documents/PacketVisualiser/dumps/asterisk_rtp_downstream_240.pcap"
    #loadingPath = "/home/student/Documents/Jupyter Demo/framework/Ekiga_test_rtp_only_downstream_filtered.pcap"
    pktsDataFloat = sFunc.readPcap(loadingPath)

    resultPkts2dArray = sFunc.byteLabelPreOperation(pktsDataFloat)
    pktsDataFloatIndex = sFunc.pktLabelGeneration(resultPkts2dArray)
    #print(pktsDataFloatIndex)
    pktsDataFloatIndex = np.array(pktsDataFloatIndex)
    print('labels statistic: ', sFunc.labelsNumStatistic(pktsDataFloatIndex))

    randomState = np.random.randint(0, 100)
    osmFlag = True
    if (osmFlag):
        osm = sFunc.overSamplingMethod(
            methodChosen='naiveRandom')  #,randomState=randomState
        X_sampled, y_sampled = osm.dataInput(pktsDataFloat, pktsDataFloatIndex)

    else:
        X_sampled = pktsDataFloat
        y_sampled = pktsDataFloatIndex

    labels = sFunc.outputLabel(y_sampled)

    if len(labels) <= 2:
        print('Binary classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag = False
    else:
        print('Multiple classification.')
        print('original labels: ', labels)
        # the most important label which controls if it's multi-classification:
        multiFlag = True

    # Split into training and test
    testSize = 0.5
    randomState = np.random.randint(0, 100)
    y_Num = sFunc.labels2Num(labels, y_sampled)

    # Use label_binarize to be multi-label like settings
    y_binarized = label_binarize(y_sampled, classes=labels)
    #print(y_binarized)
    #n_classes = y_binarized.shape[1]

    X_train, X_test, y_train, y_test = sFunc.trainTestSeparate(
        X_sampled, y_Num, testSize, random_state=randomState)
    '''
    y_train_m, y_test_m are the separate part from y_binarized.
    For nn, model needs y_train and y_test, but on calculating roc, pr 
    the plot needs y_train_m and y_test_m
    '''
    _, _, y_train_m, y_test_m = sFunc.trainTestSeparate(
        X_sampled, y_binarized, testSize, random_state=randomState)

    # print("normal class labels of 'y_test': \n", y_test)
    # print("binarized class labels of 'y_test': \n", y_test_m)

    X_train_std, X_test_std = sFunc.dataStandardization(X_train, X_test)
    _, pktsDataframe_std = sFunc.dataStandardization(X_train, pktsDataFloat)

    pkts_accuracy_list = list()

    clf_list = list()
    model_list = list()

    time_list = list()

    mlModel0 = sFunc.classifierAndItsParameters(
        MlModel='LogisticRegression',
        lr_C=1.0,
        lr_class_weight='balanced',
        lr_solver='liblinear',
        lr_multi_class='auto')
        
    numLabels = sFunc.outputLabel(y_Num)
    print('current input Num labels: ', numLabels)
    tick = time.time()
    clf0 = mlModel0.buildingModelAndFittingTrainSet(X_train_std, y_train)
    clf0.score(X_test_std, y_test)
    time_list.append(time.time() - tick)
    print('Test accuracy of Logistic Regession:', clf0.score(
        X_test_std, y_test))
    pkts_accuracy_list.append(clf0.score(X_test_std, y_test))
    clf_list.append(clf0)
    model_list.append('LogisticRegression')

    mlModel1 = sFunc.classifierAndItsParameters(
        MlModel='LinearSVC',
        lsvc_C=1.0,
        lsvc_max_iter=10000,
        lsvc_class_weight='balanced')
    tick = time.time()
    clf1 = mlModel1.buildingModelAndFittingTrainSet(X_train_std, y_train)
    
    clf1.score(X_test_std, y_test)
    time_list.append(time.time() - tick)
    print('Test accuracy of Linear SVM:', clf1.score(X_test_std, y_test))
    pkts_accuracy_list.append(clf1.score(X_test_std, y_test))
    clf_list.append(clf1)
    model_list.append('LinearSVC')

    mlModel2 = sFunc.classifierAndItsParameters(
        MlModel='SVC',
        svc_kernel='rbf',
        svc_C=1.0,
        svc_gamma='scale',
        svc_class_weight='balanced')
    tick = time.time()
    clf2 = mlModel2.buildingModelAndFittingTrainSet(X_train_std, y_train)
    
    clf2.score(X_test_std, y_test)
    time_list.append(time.time() - tick)
    print('Test accuracy of SVM:', clf2.score(X_test_std, y_test))
    pkts_accuracy_list.append(clf2.score(X_test_std, y_test))
    clf_list.append(clf2)
    model_list.append('SVC')

    mlModel3 = sFunc.classifierAndItsParameters(
        MlModel='DecisionTreeClassifier',
        dt_class_weight='balanced',
        dt_criterion='entropy')

    tick = time.time()
    clf3 = mlModel3.buildingModelAndFittingTrainSet(X_train_std, y_train)
    
    clf3.score(X_test_std, y_test)
    time_list.append(time.time() - tick)
    print('Test accuracy of Decision Tree:', clf3.score(X_test_std, y_test))
    pkts_accuracy_list.append(clf3.score(X_test_std, y_test))
    clf_list.append(clf3)
    model_list.append('DecisionTreeClassifier')

    mlModel4 = sFunc.classifierAndItsParameters(
        MlModel='RandomForestClassifier',
        rf_n_estimators=20,
        rf_class_weight='balanced')
    tick = time.time()
    clf4 = mlModel4.buildingModelAndFittingTrainSet(X_train_std, y_train)
    
    clf4.score(X_test_std, y_test)
    time_list.append(time.time() - tick)
    print('Test accuracy of Random Forest:', clf4.score(X_test_std, y_test))
    pkts_accuracy_list.append(clf4.score(X_test_std, y_test))
    clf_list.append(clf4)
    model_list.append('RandomForestClassifier')

    mlModel5 = sFunc.classifierAndItsParameters(MlModel='GaussianNB')
    tick = time.time()
    clf5 = mlModel5.buildingModelAndFittingTrainSet(X_train_std, y_train)
   
    clf5.score(X_test_std, y_test)
    time_list.append(time.time() - tick)
    print('Test accuracy of Gaussian Naive Bayes:',
          clf5.score(X_test_std, y_test))
    pkts_accuracy_list.append(clf5.score(X_test_std, y_test))
    clf_list.append(clf5)
    model_list.append('GaussianNB')

    mlModel6 = sFunc.classifierAndItsParameters(
        MlModel='KNeighborsClassifier', knn_algorithm='auto')
    
    tick = time.time()
    clf6 = mlModel6.buildingModelAndFittingTrainSet(X_train_std, y_train)
    
    clf6.score(X_test_std, y_test)

    time_list.append(time.time() - tick)
    print('Test accuracy of K Nearest Neighbors Classifier:',
          clf6.score(X_test_std, y_test))
    pkts_accuracy_list.append(clf6.score(X_test_std, y_test))
    clf_list.append(clf6)
    model_list.append('KNeighborsClassifier')

    ####### model: neural network #######
    mp = sFunc.classifierAndItsParameters(MlModel='NeuralNetwork')
    '''
        For neural network it doesn't need input train set here, only builds model in function 'buildingModelAndFittingTrainSet()'.
        Configurations use keras own commands. 
    '''
    neuralnet = mp.buildingModelAndFittingTrainSet(None, None)
    neuralnet.add(
        keras.layers.Dense(pktsDataFloat.shape[1], activation=tf.nn.selu))
    neuralnet.add(
        keras.layers.Dense(2 * pktsDataFloat.shape[1], activation=tf.nn.selu))
    neuralnet.add(
        keras.layers.Dense(4 * pktsDataFloat.shape[1], activation=tf.nn.selu))
    neuralnet.add(
        keras.layers.Dense(2 * pktsDataFloat.shape[1], activation=tf.nn.selu))
    neuralnet.add(keras.layers.Dense(2 * len(labels), activation=tf.nn.selu))
    neuralnet.add(keras.layers.Dense(len(labels), activation=tf.nn.softmax))

    tick = time.time()
    neuralnet.compile(
        optimizer='adam',
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy'])

    neuralnet.fit(X_train_std, y_train, batch_size=20, epochs=50)
    
    test_loss, test_acc = neuralnet.evaluate(X_test_std, y_test)
    time_list.append(time.time() - tick)

    print('Test accuracy of neural network:', test_acc)
    pkts_accuracy_list.append(test_acc)

    # save to csv file
    csvfile = open('acc.csv', 'w+')
    writer = csv.writer(csvfile)
    writer.writerow(pkts_accuracy_list)
    csvfile.close()

    #---neural network plot---##
    num_labels = sFunc.outputLabel(y_Num)

    #####################################
    clf_m_list = list()

    if (multiFlag):
        mlModel0_m = sFunc.classifierAndItsParameters(
            MlModel='LogisticRegression',
            multiLabel=multiFlag,
            lr_C=1.0,
            lr_class_weight='balanced',
            lr_solver='liblinear',
            lr_multi_class='auto')
        clf0_m = mlModel0_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('current input labels in OnevsRest: ', clf0_m.classes_)
        print('Test accuracy of Logistic Regession in OnevsRest:',
              clf0_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf0_m)

        mlModel1_m = sFunc.classifierAndItsParameters(
            MlModel='LinearSVC',
            multiLabel=multiFlag,
            lsvc_C=1.0,
            lsvc_max_iter=10000,
            lsvc_class_weight='balanced')
        clf1_m = mlModel1_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('Test accuracy of Linear SVM in OnevsRest:',
              clf1_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf1_m)

        mlModel2_m = sFunc.classifierAndItsParameters(
            MlModel='SVC',
            multiLabel=multiFlag,
            svc_kernel='rbf',
            svc_C=1.0,
            svc_gamma='scale',
            svc_class_weight='balanced')
        clf2_m = mlModel2_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('Test accuracy of SVM in OnevsRest:',
              clf2_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf2_m)

        mlModel3_m = sFunc.classifierAndItsParameters(
            MlModel='DecisionTreeClassifier',
            multiLabel=multiFlag,
            dt_class_weight='balanced',
            dt_criterion='entropy')
        clf3_m = mlModel3_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('Test accuracy of Decision Tree in OnevsRest:',
              clf3_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf3_m)

        mlModel4_m = sFunc.classifierAndItsParameters(
            MlModel='RandomForestClassifier',
            multiLabel=multiFlag,
            rf_n_estimators=20,
            rf_class_weight='balanced')
        clf4_m = mlModel4_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('Test accuracy of Random Forest in OnevsRest:',
              clf4_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf4_m)

        mlModel5_m = sFunc.classifierAndItsParameters(
            MlModel='GaussianNB', multiLabel=multiFlag)
        clf5_m = mlModel5_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('Test accuracy of Gaussian Naive Bayes in OnevsRest:',
              clf5_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf5_m)

        mlModel6_m = sFunc.classifierAndItsParameters(
            MlModel='KNeighborsClassifier',
            multiLabel=multiFlag,
            knn_algorithm='auto')
        clf6_m = mlModel6_m.buildingModelAndFittingTrainSet(
            X_train_std, y_train_m)
        print('Test accuracy of K Nearest Neighbors Classifier in OnevsRest:',
              clf6_m.score(X_test_std, y_test_m))
        clf_m_list.append(clf6_m)

    ################################################  PLOT  #################################################
    classLabels = ['Class 0', 'Class 1', 'Class 2']
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf0,'Logistic Regression', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf1,'Linear SVC', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf2,'SVC', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf3,'Decision Tree', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf4,'Random Forest', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf5,'Gaussian Naive Bayes', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf6,'K-nearest Neighbors', X_test_std, y_test, classLabels)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_For_NN(neuralnet, X_test_std, y_test, classLabels)

    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf0,'Logistic Regression', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf1,'Linear SVC', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf2,'SVC', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf3,'Decision Tree', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf4,'Random Forest', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf5,'Gaussian Naive Bayes', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_Generator(clf6,'K-nearest Neighbors', X_test_std, y_test, classLabels,normalizationLabel=True)
    plt.figure(figsize=(11, 6.5))
    sPlot.confusion_matrix_For_NN(neuralnet, X_test_std, y_test, classLabels,normalizationLabel=True)

    #########################################################################################################
    # if not multiFlag:
    #     plt.figure(figsize=(11, 6.5))
    #     sPlot.ROC_curve_Generator(
    #         clf2,
    #         'SVC',
    #         X_test_std,
    #         y_test,
    #         multiFlag=multiFlag,
    #         n_classes= len(labels))

    #     plt.figure(figsize=(11, 6.5))
    #     sPlot.PR_curve_Generator(
    #         clf2,
    #         'SVC',
    #         X_test_std,
    #         y_test,
    #         multiFlag=multiFlag,
    #         n_classes= len(labels))
    # else:
    #     plt.figure(figsize=(11, 6.5))
    #     sPlot.ROC_curve_Generator(
    #         clf2_m,
    #         'SVC',
    #         X_test_std,
    #         y_test_m,
    #         multiFlag=multiFlag,
    #         n_classes= len(labels))

    #     plt.figure(figsize=(11, 6.5))
    #     sPlot.PR_curve_Generator(
    #         clf2_m,
    #         'SVC',
    #         X_test_std,
    #         y_test_m,
    #         multiFlag=multiFlag,
    #         n_classes= len(labels))
    #########################################################################################################

    fpr_list = list()
    tpr_list = list()
    roc_auc_list = list()
    precision_list = list()
    recall_list = list()
    average_precision_list = list()
    f1Score_list = list()

    if (not multiFlag):
        for i, item in enumerate(clf_list):
            fpr, tpr, roc_auc=sFunc.Cal_ROC_curve_parameters(item, model_list[i],X_test_std,y_test, multiFlag=multiFlag)
            precision, recall, average_precision, f1Score=sFunc.Cal_PR_curve_parameters(item, model_list[i],X_test_std,y_test, multiFlag=multiFlag)
            fpr_list.append(fpr)
            tpr_list.append(tpr)
            roc_auc_list.append(roc_auc)
            precision_list.append(precision)
            recall_list.append(recall)
            average_precision_list.append(average_precision)
            f1Score_list.append(f1Score)

        fpr_nn, tpr_nn, roc_auc_nn = sFunc.Cal_nn_ROC_parameters(neuralnet, X_test_std, y_test_m, multiFlag=multiFlag)
        precision_nn, recall_nn, average_precision_nn, f1Score_nn=sFunc.Cal_nn_PR_parameters(neuralnet,X_test_std,y_test_m,num_labels,multiFlag=multiFlag)
    else:
        for i, item in enumerate(clf_m_list):
            fpr, tpr, roc_auc=sFunc.Cal_ROC_curve_parameters(item, model_list[i],X_test_std,y_test_m, multiFlag=multiFlag)
            precision, recall, average_precision, f1Score=sFunc.Cal_PR_curve_parameters(item, model_list[i],X_test_std,y_test_m, multiFlag=multiFlag)
            fpr_list.append(fpr)
            tpr_list.append(tpr)
            roc_auc_list.append(roc_auc)
            precision_list.append(precision)
            recall_list.append(recall)
            average_precision_list.append(average_precision)
            f1Score_list.append(f1Score)

        fpr_nn, tpr_nn, roc_auc_nn = sFunc.Cal_nn_ROC_parameters(neuralnet, X_test_std, y_test_m, multiFlag=multiFlag)
        precision_nn, recall_nn, average_precision_nn, f1Score_nn=sFunc.Cal_nn_PR_parameters(neuralnet,X_test_std,y_test_m,num_labels,multiFlag=multiFlag)

    colors = [
        'navy', 'red', 'gold', 'darkorchid', 'chartreuse', 'seagreen',
        'deepskyblue', 'darkorange', 'blue', 'violet'
    ]
    markers = ['o', 'd', 'v', 's', '<', '*', '^', '>', '+', 'D']
    linestyles = ['-', '--', '-.', ':', '-', '--', '-.', ':', '-', '--']
    nameList = [
        'Logistic Regression', 'Linear SVC', 'SVC', 'Decision Tree',
        'Random Forest', 'Gaussian Naive Bayes', 'K-nearest Neighbors'
    ]
    plt.figure(figsize=(11, 12))
    ax = plt.subplot()
    for i, color in zip(range(len(fpr_list)), colors):
        if i == 3 or i == 4 or i == 6 :
            plt.plot(
                fpr_list[i],
                tpr_list[i],
                color=color,
                lw=2.5,
                #linestyle = linestyles[i],
                marker=markers[i], markeredgecolor='k', markeredgewidth=0.9, markersize=9.0, markevery = 1,
                label='ROC curve of %s (area = %.3f)' % (nameList[i], roc_auc_list[i]))
        else:
            #pass
            plt.plot(
                fpr_list[i],
                tpr_list[i],
                color=color,
                lw=2.5,
                #linestyle = linestyles[i],
                marker=markers[i], markeredgecolor='k', markeredgewidth=0.9, markersize=9.0, markevery = 5,
                label='ROC curve of %s (area = %.3f)' % (nameList[i], roc_auc_list[i]))

    plt.plot(fpr_nn, tpr_nn,color='darkorange', lw=2.5, marker='>', markeredgecolor='k', markeredgewidth=0.9, markersize=9.0, markevery = 5, label='ROC curve of %s (area = %.3f)' %('Neural Network', roc_auc_nn))

    #plt.plot([0, 1], [0, 1], 'k--', lw=2)
    #plt.title('ROC(receiver operating characteristic) curves of different classifiers')
    plt.grid(ls='--')
    plt.xlim([-0.01, 0.2])
    plt.ylim([0.8, 1.01])
    plt.xlabel('False Positive Rate', fontsize=18)
    plt.ylabel('True Positive Rate', fontsize=18)
    plt.xticks(fontsize=17)
    plt.yticks(fontsize=17)
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.14), frameon=False,
    fancybox=False, shadow=False, markerscale=1, numpoints=2, fontsize=18)
    ##########################################################################################################
    plt.figure(figsize=(11, 12))
    ax = plt.subplot()
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(0.9, y[45] + 0.02))

    lines.append(l)

    for i, color in zip(range(len(fpr_list)), colors):
        if i == 3 or i==4 or i == 6 :
        
            l, = plt.plot(recall_list[i], precision_list[i],marker=markers[i], markeredgecolor='k', markeredgewidth=0.9, markersize=9.0, markevery = 1, color=color, lw=2.5,label= 'PR curve of %s (area = %.3f, F1 = %.3f)'
                %(nameList[i], average_precision_list[i], f1Score_list[i]))
            lines.append(l)
        else:
            l, = plt.plot(recall_list[i], precision_list[i],marker=markers[i], markeredgecolor='k', markeredgewidth=0.9, markersize=9.0, markevery = 10, color=color, lw=2.5,label= 'PR curve of %s (area = %.3f, F1 = %.3f)'
                %(nameList[i], average_precision_list[i], f1Score_list[i]))
            lines.append(l)

    l, = plt.plot(recall_nn, precision_nn,marker='>', markeredgecolor='k', markeredgewidth=0.9, markersize=9.0, markevery = 10, color= 'darkorange', lw = 2.5, label='PR curve of %s (area = %.3f, F1 = %.3f)'
            %('Neural Network', average_precision_nn, f1Score_nn))
    lines.append(l)

    #plt.title('Precision-recall curves of different classifiers')
    #plt.legend(loc="lower right")
    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.14), frameon=False,
    fancybox=False, shadow=False, markerscale=1, numpoints=2, fontsize=18)
    plt.grid(ls='--')
    plt.xlabel('Recall', fontsize=18)
    plt.ylabel('Precision', fontsize=18)
    plt.ylim([0.8, 1.01])
    plt.xlim([0.8, 1.0])
    plt.xticks(fontsize=17)
    plt.yticks(fontsize=17)

    #########################################################################################################################################################

    patterns = ('/', '\\', '.', 'o', 'x', '\\\\', '//', 'O', '*', '-', '+')
    cls_names = [
        'LR', 'L-SVC', 'SVC', 'D-Tree', 'R-Forest', 'Gauß.-NB', 'K-Neigh.',
        'NN'
    ]

    plt.figure(figsize=(11, 6.5))
    plt.rcParams['hatch.linewidth'] = 1.8
    for j in range(len(time_list)):
        plt.bar(
            cls_names[j],
            time_list[j],
            width=0.5,
            color=colors[j],
            hatch=patterns[j])
        plt.text(cls_names[j],
            time_list[j]+0.006,'%.4f' % time_list[j],ha='center', va= 'bottom',fontsize=14)
    #plt.title('Run time comparison(complexity)')
    plt.xlabel('Classifiers', fontsize=18)
    plt.ylabel('Total Running Time (s)', fontsize=18)
    plt.grid(ls='--')
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=17)

    plt.show()
    plt.pause(3)
    plt.close()
