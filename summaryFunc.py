"""
    function repository
"""

import scapy.all as scapy
import numpy as np
from sklearn.model_selection import train_test_split
import sys
import pandas as pd
import math

from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression, SGDClassifier, Perceptron, PassiveAggressiveClassifier
from sklearn import tree, svm, neighbors
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.multiclass import OneVsRestClassifier, OneVsOneClassifier
from sklearn.preprocessing import label_binarize
from sklearn.naive_bayes import MultinomialNB, BernoulliNB

from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import average_precision_score
from sklearn.metrics import f1_score

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

from imblearn.over_sampling import RandomOverSampler, SMOTE, ADASYN, BorderlineSMOTE, SVMSMOTE
from collections import Counter
import warnings
import scipy.stats

# old version used pd.dataframe

# def readPcap(path):
#     pkts = scapy.rdpcap(path)

#     for pktsIndex in range(len(pkts)):
#         pkt = pkts[pktsIndex]
#         pktBytes = bytes(pkt)
#         if pktsIndex == 0:
#             pktBytearray = bytearray(pktBytes)
#         else:
#             pktBytearrayAdding = bytearray(pktBytes)
#             pktBytearray = np.vstack((pktBytearray, pktBytearrayAdding))

#     pkts2dData = pd.DataFrame(pktBytearray)
#     pktsDataframeFloat = pkts2dData.astype(float)
#     return pktsDataframeFloat

# new version used np.array, more efficient


def readPcap(path):
    pkts = scapy.rdpcap(path)

    bytesArray = np.zeros(shape=(len(pkts), len(pkts[0])))
    for pktsIndex, pktItem in enumerate(pkts):
        pktBytes = bytes(pktItem)
        for i in range(len(pktBytes)):
            bytesArray[pktsIndex, i] = float(pktBytes[i])
    
    return bytesArray


def readPcapDiffPktsLength(path, max_length=400):
    pkts = scapy.rdpcap(path)
    length = max_length
    bytesArray = np.zeros(shape=(len(pkts), length))
    for pktsIndex, pktItem in enumerate(pkts):
        pktBytes = bytes(pktItem)
        for i in range(len(pktBytes)):
            bytesArray[pktsIndex, i] = float(pktBytes[i])

    # pkts2dData = pd.DataFrame(bytesArray)
    return bytesArray

def byte2HalfWord(pkts2dData):
    pkts2dDataInt= pkts2dData.astype(int)
    hWords = np.zeros(shape=(len(pkts2dDataInt), math.ceil(pkts2dDataInt.shape[1]/2.0)))
    for i in range(len(pkts2dDataInt)):
        for j in range(0, pkts2dDataInt.shape[1], 2):
            column = int( j / 2)
            if j+1 > pkts2dDataInt.shape[1]-1:
                hWords[i][column] = pkts2dDataInt[i][j] << 8
            else:
                hWords[i][column] = pkts2dDataInt[i][j] << 8 | pkts2dDataInt[i][j+1]
    
    return hWords.astype(float)

def byte2Word(pkts2dData):
    pkts2dDataInt= pkts2dData.astype(int)
    words = np.zeros(shape=(len(pkts2dDataInt), math.ceil(pkts2dDataInt.shape[1]/4.0)))
    for i in range(len(pkts2dDataInt)):
        for j in range(0, pkts2dDataInt.shape[1], 4):
            column = int( j / 4)
            if j +3 > pkts2dDataInt.shape[1]-1:
                if j+3-(pkts2dDataInt.shape[1]-1) ==1:
                    words[i][column] = pkts2dDataInt[i][j] << 24 | pkts2dDataInt[i][j+1] << 16 | pkts2dDataInt[i][j+2] << 8 
                elif j+3-(pkts2dDataInt.shape[1]-1) ==2:
                    words[i][column] = pkts2dDataInt[i][j] << 24 | pkts2dDataInt[i][j+1] << 16
                elif j+3-(pkts2dDataInt.shape[1]-1) ==3:
                    words[i][column] = pkts2dDataInt[i][j] << 24
            else:
                words[i][column] = (pkts2dDataInt[i][j] << 24) | (pkts2dDataInt[i][j+1] << 16) | (pkts2dDataInt[i][j+2] << 8) | (pkts2dDataInt[i][j+3] )
    
    return words.astype(float)
            



def byteLabelPreOperation(pkts2dData):
    resultPkts2dArray = np.zeros([len(pkts2dData), pkts2dData.shape[1]])
    '''
    In the package data preparation,
    0 means static;
    1 means increment;
    2 means decrement;
    3 means random.
    '''
     
    for columnIndex in range(pkts2dData.shape[1]):
        NumOfIncr = 0
        NumOfDecr = 0
        NumOfCons = 0
        Incr = False
        Decr = False
        Cons = False
        # 'changingNum' means the byte info changing times, except the first packet.
        changingNum = 0
        for rowIndex in range(len(pkts2dData)):
            if rowIndex == 0:
                pass
            else:
                if pkts2dData[rowIndex, columnIndex] == pkts2dData[rowIndex - 1, columnIndex]:
                    if rowIndex == 1:
                        resultPkts2dArray[rowIndex - 1, columnIndex] = 0
                        NumOfCons += 1
                        Cons = True
                    resultPkts2dArray[rowIndex, columnIndex] = 0
                    NumOfCons += 1
                    Cons = True
                elif pkts2dData[rowIndex, columnIndex] > pkts2dData[rowIndex - 1, columnIndex]:
                    if ((pkts2dData[rowIndex, columnIndex] == 255) or
                        (pkts2dData[rowIndex, columnIndex] == 254)) and (
                            (pkts2dData[rowIndex - 1, columnIndex] == 0) or (pkts2dData[rowIndex - 1, columnIndex] == 1)):
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 2
                        NumOfDecr += 1
                        Decr = True
                    else:
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 1
                        NumOfIncr += 1
                        Incr = True
                else:
                    if ((pkts2dData[rowIndex, columnIndex] == 0) or (pkts2dData[rowIndex, columnIndex] == 1)) and (
                        (pkts2dData[rowIndex - 1, columnIndex] == 255) or
                        (pkts2dData[rowIndex - 1, columnIndex] == 254)):
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 1
                        NumOfIncr += 1
                        Incr = True
                    else:
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 2
                        NumOfDecr += 1
                        Decr = True

            if rowIndex == 0:
                if resultPkts2dArray[rowIndex, columnIndex] == 0:
                    posiState = 0
                elif resultPkts2dArray[rowIndex, columnIndex] == 1:
                    posiState = 1
                elif resultPkts2dArray[rowIndex, columnIndex] == 2:
                    posiState = 2

            if rowIndex > 0:
                if resultPkts2dArray[rowIndex, columnIndex] == posiState:
                    pass
                else:
                    posiState = resultPkts2dArray[rowIndex, columnIndex]
                    changingNum += 1
        '''
            'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
        '''
        if (Incr and Decr) and (changingNum > 2):
            resultPkts2dArray[:, columnIndex] = 3
        elif (Cons and Incr and not Decr):

            print('changingNum: %d, Incr columnIndex: %d, changing percentage: %f,' %
                  (changingNum, columnIndex, float(changingNum) / len(pkts2dData)))
            if float(changingNum) / len(pkts2dData) >= 0.5:
                resultPkts2dArray[:, columnIndex] = 3
                print(
                    'due to changing percentage bigger than 0.5, this column was set as random.'
                )
            elif (0.05<=float(changingNum) / len(pkts2dData) < 0.5):
                resultPkts2dArray[:, columnIndex] = 1
                # if changing percentage is smaller than 0.5, we could get bigger gain than setting this column directly as random.
                print(
                    'due to changing percentage smaller than 0.5 and bigger than 0.05, this column was still incremental.'
                )
            else:
                #pass
                incrIndexList = []
                for i, item in enumerate(resultPkts2dArray[:, columnIndex]):
                    if item == 1:
                        incrIndexList.append(i)
                for ii, iItem in enumerate(incrIndexList):
                    if iItem+3<=len(pkts2dData)-1:
                        resultPkts2dArray[:, columnIndex][iItem+1]=1
                        resultPkts2dArray[:, columnIndex][iItem+2]=1
                        resultPkts2dArray[:, columnIndex][iItem+3]=1
                    elif (iItem+2<=len(pkts2dData)-1) and (iItem+3>len(pkts2dData)-1):
                        resultPkts2dArray[:, columnIndex][iItem+1]=1
                        resultPkts2dArray[:, columnIndex][iItem+2]=1
                    elif (iItem+1<=len(pkts2dData)-1) and (iItem+2>len(pkts2dData)-1):
                        resultPkts2dArray[:, columnIndex][iItem+1]=1
                    else:
                        pass
                print(
                    'due to changing percentage smaller than 0.05, keep the original labels.'
                )
                # print(resultPkts2dArray[:, columnIndex])

        elif (Cons and Decr and not Incr):

            print('changingNum: %d, Decr columnIndex: %d, changing percentage: %f,' %
                  (changingNum, columnIndex, float(changingNum) / len(pkts2dData)))
            if float(changingNum) / len(pkts2dData)>= 0.5:
                resultPkts2dArray[:, columnIndex] = 3
                print(
                    'due to changing percentage bigger than 0.5, this column was set as random.'
                )
            elif (0.05<=float(changingNum) / len(pkts2dData) < 0.5):
                resultPkts2dArray[:, columnIndex] = 2
                # if changing percentage is smaller than 0.5, we could get bigger gain than setting this column directly as random.
                print(
                    'due to changing percentage smaller than 0.5 and bigger than 0.05, this column was still decremental.'
                )
            else:
                #pass
                decrIndexList = []
                for i, item in enumerate(resultPkts2dArray[:, columnIndex]):
                    if item == 2:
                        decrIndexList.append(i)
                for ii, iItem in enumerate(decrIndexList):
                    if iItem+3<=len(pkts2dData)-1:
                        resultPkts2dArray[:, columnIndex][iItem+1]=2
                        resultPkts2dArray[:, columnIndex][iItem+2]=2
                        resultPkts2dArray[:, columnIndex][iItem+3]=2
                    elif (iItem+2<=len(pkts2dData)-1) and (iItem+3>len(pkts2dData)-1):
                        resultPkts2dArray[:, columnIndex][iItem+1]=2
                        resultPkts2dArray[:, columnIndex][iItem+2]=2
                    elif (iItem+1<=len(pkts2dData)-1) and (iItem+2>len(pkts2dData)-1):
                        resultPkts2dArray[:, columnIndex][iItem+1]=2
                    else:
                        pass
                print(
                    'due to changing percentage smaller than 0.05, keep the original labels.'
                )
                # print(resultPkts2dArray[:, columnIndex])
    # print("result Pkts 2d Array: ", resultPkts2dArray)
    return resultPkts2dArray


def byteLabelPreOperationOptimized(pkts2dData):
    resultPkts2dArray = np.zeros([len(pkts2dData), pkts2dData.shape[1]])
    '''
    In the package data preparation,
    0 means static;
    1 means increment;
    2 means decrement;
    3 means random.
    '''
    for columnIndex in range(pkts2dData.shape[1]):
        NumOfIncr = 0
        NumOfDecr = 0
        NumOfCons = 0
        Incr = False
        Decr = False
        Cons = False
        # 'changingNum' means the byte info changing times, except the first packet.
        changingNum = 0
        for rowIndex in range(len(pkts2dData)):
            if rowIndex == 0:
                pass
            else:
                if pkts2dData[rowIndex, columnIndex] == pkts2dData[
                        rowIndex - 1, columnIndex]:
                    if rowIndex == 1:
                        resultPkts2dArray[rowIndex - 1, columnIndex] = 0
                        NumOfCons += 1
                        Cons = True
                    resultPkts2dArray[rowIndex, columnIndex] = 0
                    NumOfCons += 1
                    Cons = True
                elif pkts2dData[rowIndex, columnIndex] > pkts2dData[
                        rowIndex - 1, columnIndex]:
                    if ((pkts2dData[rowIndex, columnIndex] == 255) or
                        (pkts2dData[rowIndex, columnIndex] == 254)) and (
                            pkts2dData[rowIndex - 1, columnIndex] == 0):
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 2
                        NumOfDecr += 1
                        Decr = True
                    else:
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 1
                        NumOfIncr += 1
                        Incr = True
                else:
                    if (pkts2dData[rowIndex, columnIndex] == 0) and (
                        (pkts2dData[rowIndex - 1, columnIndex] == 255) or
                        (pkts2dData[rowIndex - 1, columnIndex] == 254)):
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 1
                            NumOfIncr += 1
                            Incr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 1
                        NumOfIncr += 1
                        Incr = True
                    else:
                        if rowIndex == 1:
                            resultPkts2dArray[rowIndex - 1, columnIndex] = 2
                            NumOfDecr += 1
                            Decr = True
                        resultPkts2dArray[rowIndex, columnIndex] = 2
                        NumOfDecr += 1
                        Decr = True

            if rowIndex == 1:
                if resultPkts2dArray[rowIndex, columnIndex] == 0:
                    posiState = 0
                elif resultPkts2dArray[rowIndex, columnIndex] == 1:
                    posiState = 1
                elif resultPkts2dArray[rowIndex, columnIndex] == 2:
                    posiState = 2

            if rowIndex > 1:
                if resultPkts2dArray[rowIndex, columnIndex] == posiState:
                    pass
                else:
                    posiState = resultPkts2dArray[rowIndex, columnIndex]
                    changingNum += 1
        '''
            'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
        '''
        if (Incr and Decr) and (changingNum > 2):
            resultPkts2dArray[:, columnIndex] = 3
            # incrPercentage = float(NumOfIncr) / len(pkts2dData)
            # decrPercentage = float(NumOfDecr) / len(pkts2dData)
            # consPercentage = float(NumOfCons) / len(pkts2dData)
            # print("random column's Index : ", columnIndex,
            #       '; Total quantity of this column elements: ',
            #       len(pkts2dData))
            # print('posiStateChangingTimes: ', changingNum)
            # print('Increment number: ', NumOfIncr, '; Percentage: ',
            #       incrPercentage)
            # print('Decrement number: ', NumOfDecr, '; Percentage: ',
            #       decrPercentage)
            # print('Constant number: ', NumOfCons, '; Percentage: ',
            #       consPercentage)
        elif (Cons and Incr and not Decr) and (changingNum > 1):
            print('Incr columnIndex: %d, changing percentage: %f,' %
                  (columnIndex, float(changingNum) / len(pkts2dData)))
            if float(changingNum) / len(pkts2dData) >= 0.5:
                resultPkts2dArray[:, columnIndex] = 3
                print(
                    'due to changing percentage bigger than 0.5, this column was set as random.'
                )
            else:
                resultPkts2dArray[:, columnIndex] = 1
                # if changing percentage is smaller than 0.5, we could get bigger gain than setting this column directly as random.
                print(
                    'due to changing percentage smaller than 0.5, this column was still incremental.'
                )
        elif (Cons and Decr and not Incr) and (changingNum > 1):
            print('Decr columnIndex: %d, percentage: %f,' %
                  (columnIndex, float(changingNum) / len(pkts2dData)))
            if float(changingNum) / len(pkts2dData) >= 0.5:
                resultPkts2dArray[:, columnIndex] = 3
                print(
                    'due to changing percentage bigger than 0.5, this column was set as random.'
                )
            else:
                resultPkts2dArray[:, columnIndex] = 2
                # if changing percentage is smaller than 0.5, we could get bigger gain than setting this column directly as random.
                print(
                    'due to changing percentage smaller than 0.5, this column was still decremental.'
                )

        ############ Version 1 #############
        # if ((Incr and Decr) or (Incr and Cons) or
        #     (Cons and Decr)) and (changingNum > 2):
        #     resultPkts2dArray[:, columnIndex] = 3
        #     incrPercentage = float(NumOfIncr) / len(pkts2dData)
        #     decrPercentage = float(NumOfDecr) / len(pkts2dData)
        #     consPercentage = float(NumOfCons) / len(pkts2dData)
        #     # print("random column's Index : ", columnIndex,
        #     #       '; Total quantity of this column elements: ',
        #     #       len(pkts2dData))
        #     # print('posiStateChangingTimes: ', changingNum)
        #     # print('Increment number: ', NumOfIncr, '; Percentage: ',
        #     #       incrPercentage)
        #     # print('Decrement number: ', NumOfDecr, '; Percentage: ',
        #     #       decrPercentage)
        #     # print('Constant number: ', NumOfCons, '; Percentage: ',
        #     #       consPercentage)

        ############ Version 2 #############
        # '''
        #     'changingNum<=2' limits the continuity, if changingNum is big, then the byte position random
        # '''
        # if (Incr and Decr) and (changingNum > 2):
        #     resultPkts2dArray[:, columnIndex] = 3
        #     # incrPercentage = float(NumOfIncr) / len(pkts2dData)
        #     # decrPercentage = float(NumOfDecr) / len(pkts2dData)
        #     # consPercentage = float(NumOfCons) / len(pkts2dData)
        #     # print("random column's Index : ", columnIndex,
        #     #       '; Total quantity of this column elements: ',
        #     #       len(pkts2dData))
        #     # print('posiStateChangingTimes: ', changingNum)
        #     # print('Increment number: ', NumOfIncr, '; Percentage: ',
        #     #       incrPercentage)
        #     # print('Decrement number: ', NumOfDecr, '; Percentage: ',
        #     #       decrPercentage)
        #     # print('Constant number: ', NumOfCons, '; Percentage: ',
        #     #       consPercentage)
        # elif (Cons and Incr and not Decr) and (changingNum > 1):
        #     print('Incr columnIndex: %d, changing percentage: %f,' %
        #           (columnIndex, float(changingNum) / len(pkts2dData)))
        #     if float(changingNum) / len(pkts2dData) >= 0.5:
        #         resultPkts2dArray[:, columnIndex] = 3
        #         print(
        #             'due to changing percentage bigger than 0.5, this column was set as random.'
        #         )
        #     else:
        #         resultPkts2dArray[:, columnIndex] = 1
        #         # if changing percentage is smaller than 0.5, we could get bigger gain than setting this column directly as random.
        #         print(
        #             'due to changing percentage smaller than 0.5, this column was still incremental.'
        #         )
        # elif (Cons and Decr and not Incr) and (changingNum > 1):
        #     print('Decr columnIndex: %d, percentage: %f,' %
        #           (columnIndex, float(changingNum) / len(pkts2dData)))
        #     if float(changingNum) / len(pkts2dData) >= 0.5:
        #         resultPkts2dArray[:, columnIndex] = 3
        #         print(
        #             'due to changing percentage bigger than 0.5, this column was set as random.'
        #         )
        #     else:
        #         resultPkts2dArray[:, columnIndex] = 2
        #         # if changing percentage is smaller than 0.5, we could get bigger gain than setting this column directly as random.
        #         print(
        #             'due to changing percentage smaller than 0.5, this column was still decremental.'
        #         )

    # print("result Pkts 2d Array: ", resultPkts2dArray)
    return resultPkts2dArray


def addingPktsIndex(dataFrame, labelName, startIndex=0, insertingPosition=0):
    posiList = range(startIndex, startIndex + len(dataFrame))
    newDataFrame = dataFrame.copy()
    newDataFrame.insert(insertingPosition, labelName, posiList)
    return newDataFrame.astype(float)


def pktLabelGeneration(resultPkts2dArray):
    indexArrayOfPktsDataframe = ["" for x in range(len(resultPkts2dArray))]
    #["" for x in range(len(pktsDataFloat))]
    #np.empty(len(pktsDataFloat), dtype = str)

    resultPkts2dArrayInt = resultPkts2dArray.astype(int)
    #print(resultPkts2dArrayInt)
    for i in range(resultPkts2dArrayInt.shape[0]):
        resultPkts2dArrayStr = [
            str(elemt) for elemt in resultPkts2dArrayInt[i]
        ]
        indexArrayOfPktsDataframe[i] = "".join(resultPkts2dArrayStr)

    return indexArrayOfPktsDataframe


def labels2Num(labels, arrayInput):

    labelsToNum = dict()
    for i, item in enumerate(labels):
        labelsToNum[item] = float(i)
    print('labels2Num dict: ', labelsToNum)
    NUM_list = list()
    for j, j_item in enumerate(arrayInput):
        NUM_list.append(labelsToNum[j_item])
    array = np.array(NUM_list)
    return array


def trainTestSeparate(X, y, test_size, random_state=None):
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=test_size, random_state=random_state)
    return X_train, X_test, y_train, y_test


def sequentialTrainTestSeperate(X, y, pktSize, test_size):
    train_size = 1.0 - test_size
    # train_pkts_size = math.ceil(len(X) * train_size / pktSize) # byte sample
    # train_length = train_pkts_size * pktSize # byte sample
    train_pkts_size = math.ceil(len(X) * train_size) # packet sample
    train_length = train_pkts_size # packet sample
    X_train = X[:train_length, :]
    X_test = X[train_length:, :]

    if (y.ndim == 1):
        y_train = y[:train_length]
        y_test = y[train_length:]
    else:
        y_train = y[:train_length, :]
        y_test = y[train_length:, :]
    return X_train, X_test, y_train, y_test


def dataStandardization(X0, X1):
    '''
    from sklearn.preprocessing import StandardScaler

    data standardlized, compute the mean and standard deviation on a training set(X0) 
    so as to be able to later reapply the same transformation on the testing set(X1).
    '''
    sc = StandardScaler()
    sc.fit(X0)
    X0_std = sc.transform(X0)
    X1_std = sc.transform(X1)
    return X0_std, X1_std


def dataStandardization_singleSet(X0):
    '''
    from sklearn.preprocessing import StandardScaler
    '''
    sc = StandardScaler()
    sc.fit(X0)
    X0_std = sc.transform(X0)
    return X0_std


class overSamplingMethod():
    """over Sampling methods.

    Parameters
    ----------
    methodChosen: 
    
    1. keyword 'naiveRandom' indicates Naive random over-sampling.
    2. keyword 'SMOTE' indicates SMOTE over-sampling.
    3. keyword 'ADASYN' indicates ADASYN over-sampling.
    4. keyword 'BorderlineSMOTE' indicates BorderlineSMOTE over-sampling, in addtion the parameter 'kind' should be also input(i.e 'borderline-1' or 'borderline-2').
    5. keyword 'SVMSMOTE' indicates SVMSMOTE over-sampling.
    """

    def __init__(self,
                 methodChosen='naiveRandom',
                 BlS_kind='borderline-1',
                 randomState=None):
        self.methodChosen = methodChosen
        self.BlS_kind = BlS_kind
        self.randomState = randomState

    def dataInput(self, X, y):
        if (self.methodChosen == 'naiveRandom'):
            return self.overSampNaiveRandom(X, y)
        elif (self.methodChosen == 'ADASYN'):
            return self.overSampADASYN(X, y)
        elif (self.methodChosen == 'SMOTE'):
            return self.overSampSMOTE(X, y)
        elif (self.methodChosen == 'BorderlineSMOTE'):
            return self.overSampBorderlineSMOTE(X, y)
        elif (self.methodChosen == 'SVMSMOTE'):
            return self.overSampSVMSMOTE(X, y)
        else:
            warnings.warn('Wrong chose, no such over sampling method!')
            sys.exit(0)

    def overSampNaiveRandom(self, X, y):
        X_resampled, y_resampled = RandomOverSampler(
            random_state=self.randomState).fit_resample(X, y)
        if y.ndim == 1:
            print('original data: ', sorted(Counter(y).items()))
            print('data over sampling by naive random: ',
                  sorted(Counter(y_resampled).items()))
        else:
            pass
        return X_resampled, y_resampled

    def overSampADASYN(self, X, y):
        X_resampled, y_resampled = ADASYN(
            random_state=self.randomState).fit_resample(X, y)
        if y.ndim == 1:
            print('original data: ', sorted(Counter(y).items()))
            print('data over sampling by ADASYN: ',
                  sorted(Counter(y_resampled).items()))
        else:
            pass
        return X_resampled, y_resampled

    def overSampSMOTE(self, X, y):
        X_resampled, y_resampled = SMOTE(
            random_state=self.randomState).fit_resample(X, y)
        if y.ndim == 1:
            print('original data: ', sorted(Counter(y).items()))
            print('data over sampling by SMOTE: ',
                  sorted(Counter(y_resampled).items()))
        else:
            pass
        return X_resampled, y_resampled

    def overSampBorderlineSMOTE(self, X, y):
        X_resampled, y_resampled = BorderlineSMOTE(
            random_state=self.randomState, kind=self.BlS_kind).fit_resample(
                X, y)
        if y.ndim == 1:
            print('original data: ', sorted(Counter(y).items()))
            print('data over sampling by BorderlineSMOTE: ',
                  sorted(Counter(y_resampled).items()))
        else:
            pass
        return X_resampled, y_resampled

    def overSampSVMSMOTE(self, X, y):
        X_resampled, y_resampled = SVMSMOTE(
            random_state=self.randomState).fit_resample(X, y)
        if y.ndim == 1:
            print('original data: ', sorted(Counter(y).items()))
            print('data over sampling by SVMSMOTE: ',
                  sorted(Counter(y_resampled).items()))
        else:
            pass
        return X_resampled, y_resampled


def outputLabel(y):
    return sorted(Counter(y))


def labelsNumStatistic(y):
    return sorted(Counter(y).items())


class classifierAndItsParameters():
    """ensure machine learning classification method and its corresponding parameters.

    Parameters
    ----------
    MlModel: 
    1. Keyword 'LogisticRegression' indicates LogisticRegression.
            Correspondingly these parameters with head 'lr_' denote the inputs of model LogisticRegression, 
            especially 'lr_sample_weight' belongs fit function.
    2. keyword 'LinearSVC' indicates svm.LinearSVC.
            Correspondingly these parameters with head 'lsvc_' denote the inputs of model LinearSVC, 
            especially 'lsvc_sample_weight' belongs fit function.
    3. keyword 'SVC' indicates svm.SVC.
            Correspondingly these parameters with head 'svc_' denote the inputs of model SVC, 
            especially 'svc_sample_weight' belongs fit function.
    4. keyword 'DecisionTreeClassifier' indicates tree.DecisionTreeClassifier.
            Correspondingly these parameters with head 'dt_' denote the inputs of model DecisionTreeClassifier, 
            especially 'dt_sample_weight', 'dt_check_input', 'dt_X_idx_sorted' belong fit function.
    5. keyword 'RandomForestClassifier' indicates RandomForestClassifier.
            Correspondingly these parameters with head 'rf_' denote the inputs of model RandomForestClassifier, 
            especially 'rf_sample_weight' belongs fit function.
    6. keyword 'GaussianNB' indicates Gaussian naive Bayes.
            Correspondingly these parameters with head 'gnb_' denote the inputs of model GaussianNB, 
            especially 'gnb_sample_weight' belongs fit function.
    7. keyword 'BernoulliNB' indicates Bernoulli naive Bayes.
            Correspondingly these parameters with head 'bnb_' denote the inputs of model BernoulliNB, 
            especially 'bnb_sample_weight' belongs fit function.
    8. keyword 'KNeighborsClassifier' indicates neighbors.KNeighborsClassifier.
            Correspondingly these parameters with head 'knn_' denote the inputs of model KNeighborsClassifier, 
            the fit function of KNeighborsClassifier doesn't need special input parameters.
    9. keyword 'NeuralNetwork' indicates neural network keras.Sequential.
            In 'NeuralNetwork' model, no parameters need to be inputted, just builds model keras.Sequential. 
            Model configurations would be done in current py file by keras own command.

    """

    def __init__(self,
                 MlModel='LogisticRegression',
                 multiLabel=False,
                 multi_n_jobs=None,
                 lr_penalty='l2',
                 lr_dual=False,
                 lr_tol=1e-4,
                 lr_C=1.0,
                 lr_fit_intercept=True,
                 lr_intercept_scaling=1,
                 lr_class_weight=None,
                 lr_random_state=None,
                 lr_solver='warn',
                 lr_max_iter=100,
                 lr_multi_class='warn',
                 lr_verbose=0,
                 lr_warm_start=False,
                 lr_n_jobs=None,
                 lr_sample_weight=None,
                 lsvc_penalty='l2',
                 lsvc_loss='squared_hinge',
                 lsvc_dual=True,
                 lsvc_tol=1e-4,
                 lsvc_C=1.0,
                 lsvc_multi_class='ovr',
                 lsvc_fit_intercept=True,
                 lsvc_intercept_scaling=1,
                 lsvc_class_weight=None,
                 lsvc_verbose=0,
                 lsvc_random_state=None,
                 lsvc_max_iter=1000,
                 lsvc_sample_weight=None,
                 svc_C=1.0,
                 svc_kernel='rbf',
                 svc_degree=3,
                 svc_gamma='auto_deprecated',
                 svc_coef0=0.0,
                 svc_shrinking=True,
                 svc_probability=False,
                 svc_tol=0.001,
                 svc_cache_size=200,
                 svc_class_weight=None,
                 svc_verbose=False,
                 svc_max_iter=-1,
                 svc_decision_function_shape='ovr',
                 svc_random_state=None,
                 svc_sample_weight=None,
                 dt_criterion="gini",
                 dt_splitter="best",
                 dt_max_depth=None,
                 dt_min_samples_split=2,
                 dt_min_samples_leaf=1,
                 dt_min_weight_fraction_leaf=0.,
                 dt_max_features=None,
                 dt_random_state=None,
                 dt_max_leaf_nodes=None,
                 dt_min_impurity_decrease=0.,
                 dt_min_impurity_split=None,
                 dt_class_weight=None,
                 dt_presort=False,
                 dt_sample_weight=None,
                 dt_check_input=True,
                 dt_X_idx_sorted=None,
                 rf_n_estimators='warn',
                 rf_criterion="gini",
                 rf_max_depth=None,
                 rf_min_samples_split=2,
                 rf_min_samples_leaf=1,
                 rf_min_weight_fraction_leaf=0.,
                 rf_max_features="auto",
                 rf_max_leaf_nodes=None,
                 rf_min_impurity_decrease=0.,
                 rf_min_impurity_split=None,
                 rf_bootstrap=True,
                 rf_oob_score=False,
                 rf_n_jobs=None,
                 rf_random_state=None,
                 rf_verbose=0,
                 rf_warm_start=False,
                 rf_class_weight=None,
                 rf_sample_weight=None,
                 gnb_priors=None,
                 gnb_var_smoothing=1e-9,
                 gnb_sample_weight=None,
                 bnb_alpha=1.0,
                 bnb_binarize=0.0,
                 bnb_fit_prior=True,
                 bnb_class_prior=None,
                 bnb_sample_weight=None,
                 knn_n_neighbors=5,
                 knn_weights='uniform',
                 knn_algorithm='auto',
                 knn_leaf_size=30,
                 knn_p=2,
                 knn_metric='minkowski',
                 knn_metric_params=None,
                 knn_n_jobs=None):
        '''
            According parameter 'MlModel' selecting classification model
        '''
        self.MlModel = MlModel
        '''
            According parameter 'multiLabel' selecting normal label or multi-label classification, 
            'multi_n_jobs' is the unique additional parameter in multiclass(OneVsRestClassifier). 
        '''
        self.multiLabel = multiLabel
        self.multi_n_jobs = multi_n_jobs

        if self.MlModel == 'LogisticRegression':
            '''
                control the LogisticRegression model parameters
            '''
            self.lr_penalty = lr_penalty
            self.lr_dual = lr_dual
            self.lr_tol = lr_tol
            self.lr_C = lr_C
            self.lr_fit_intercept = lr_fit_intercept
            self.lr_intercept_scaling = lr_intercept_scaling
            self.lr_class_weight = lr_class_weight
            self.lr_random_state = lr_random_state
            self.lr_solver = lr_solver
            self.lr_max_iter = lr_max_iter
            self.lr_multi_class = lr_multi_class
            self.lr_verbose = lr_verbose
            self.lr_warm_start = lr_warm_start
            self.lr_n_jobs = lr_n_jobs
            '''
                control the parameters of fit function of LogisticRegression
            '''
            self.lr_sample_weight = lr_sample_weight

        elif self.MlModel == 'LinearSVC':
            '''
                control the LinearSVC model parameters
            '''
            self.lsvc_dual = lsvc_dual
            self.lsvc_tol = lsvc_tol
            self.lsvc_C = lsvc_C
            self.lsvc_multi_class = lsvc_multi_class
            self.lsvc_fit_intercept = lsvc_fit_intercept
            self.lsvc_intercept_scaling = lsvc_intercept_scaling
            self.lsvc_class_weight = lsvc_class_weight
            self.lsvc_verbose = lsvc_verbose
            self.lsvc_random_state = lsvc_random_state
            self.lsvc_max_iter = lsvc_max_iter
            self.lsvc_penalty = lsvc_penalty
            self.lsvc_loss = lsvc_loss
            '''
                control the parameters of fit function of LinearSVC
            '''
            self.lsvc_sample_weight = lsvc_sample_weight

        elif self.MlModel == 'SVC':
            '''
                control the SVC model parameters
            '''
            self.svc_C = svc_C
            self.svc_kernel = svc_kernel
            self.svc_degree = svc_degree
            self.svc_gamma = svc_gamma
            self.svc_coef0 = svc_coef0
            self.svc_shrinking = svc_shrinking
            self.svc_probability = svc_probability
            self.svc_tol = svc_tol
            self.svc_cache_size = svc_cache_size
            self.svc_class_weight = svc_class_weight
            self.svc_verbose = svc_verbose
            self.svc_max_iter = svc_max_iter
            self.svc_decision_function_shape = svc_decision_function_shape
            self.svc_random_state = svc_random_state
            '''
                control the parameters of fit function of SVC
            '''
            self.svc_sample_weight = svc_sample_weight

        elif self.MlModel == 'DecisionTreeClassifier':
            '''
                control the DecisionTreeClassifier model parameters
            '''
            self.dt_criterion = dt_criterion
            self.dt_splitter = dt_splitter
            self.dt_max_depth = dt_max_depth
            self.dt_min_samples_split = dt_min_samples_split
            self.dt_min_samples_leaf = dt_min_samples_leaf
            self.dt_min_weight_fraction_leaf = dt_min_weight_fraction_leaf
            self.dt_max_features = dt_max_features
            self.dt_max_leaf_nodes = dt_max_leaf_nodes
            self.dt_class_weight = dt_class_weight
            self.dt_random_state = dt_random_state
            self.dt_min_impurity_decrease = dt_min_impurity_decrease
            self.dt_min_impurity_split = dt_min_impurity_split
            self.dt_presort = dt_presort
            '''
                control the parameters of fit function of DecisionTreeClassifier
            '''
            self.dt_sample_weight = dt_sample_weight
            self.dt_check_input = dt_check_input
            self.dt_X_idx_sorted = dt_X_idx_sorted

        elif self.MlModel == 'RandomForestClassifier':
            '''
                control the RandomForestClassifier model parameters
            '''
            self.rf_n_estimators = rf_n_estimators
            self.rf_criterion = rf_criterion
            self.rf_max_depth = rf_max_depth
            self.rf_min_samples_split = rf_min_samples_split
            self.rf_min_samples_leaf = rf_min_samples_leaf
            self.rf_min_weight_fraction_leaf = rf_min_weight_fraction_leaf
            self.rf_max_features = rf_max_features
            self.rf_max_leaf_nodes = rf_max_leaf_nodes
            self.rf_min_impurity_decrease = rf_min_impurity_decrease
            self.rf_min_impurity_split = rf_min_impurity_split
            self.rf_bootstrap = rf_bootstrap
            self.rf_oob_score = rf_oob_score
            self.rf_n_jobs = rf_n_jobs
            self.rf_random_state = rf_random_state
            self.rf_verbose = rf_verbose
            self.rf_warm_start = rf_warm_start
            self.rf_class_weight = rf_class_weight
            '''
                control the parameters of fit function of RandomForestClassifier
            '''
            self.rf_sample_weight = rf_sample_weight

        elif self.MlModel == 'GaussianNB':
            '''
                control the GaussianNB model parameters
            '''
            self.gnb_priors = gnb_priors
            self.gnb_var_smoothing = gnb_var_smoothing
            '''
                control the parameters of fit function of GaussianNB
            '''
            self.gnb_sample_weight = gnb_sample_weight

        elif self.MlModel == 'BernoulliNB':
            '''
                control the BernoulliNB model parameters
            '''
            self.bnb_alpha = bnb_alpha
            self.bnb_binarize = bnb_binarize
            self.bnb_fit_prior = bnb_fit_prior
            self.bnb_class_prior = bnb_class_prior
            '''
                control the parameters of fit function of BernoulliNB
            '''
            self.bnb_sample_weight = bnb_sample_weight

        elif self.MlModel == 'KNeighborsClassifier':
            '''
                control the KNeighborsClassifier model parameters
            '''
            self.knn_n_neighbors = knn_n_neighbors
            self.knn_weights = knn_weights
            self.knn_algorithm = knn_algorithm
            self.knn_leaf_size = knn_leaf_size
            self.knn_p = knn_p
            self.knn_metric = knn_metric
            self.knn_metric_params = knn_metric_params
            self.knn_n_jobs = knn_n_jobs

        elif self.MlModel == 'NeuralNetwork':
            pass

        else:
            warnings.warn(
                "The chosen ML model doesn't exist, check if it's wrong model name!"
            )
            exit()

    def buildingModelAndFittingTrainSet(self, X, y):
        '''
        When "self.MlModel == 'NeuralNetwork'", train set (X, y) is not necessary.
        So just directly set inputs (X, y) as (None, None) in buildingModelAndFittingTrainSet(). 
        '''
        if (self.MlModel == 'LogisticRegression'):
            return self.modelLogisticRegression(X, y)
        elif (self.MlModel == 'LinearSVC'):
            return self.modelLinearSVC(X, y)
        elif (self.MlModel == 'SVC'):
            return self.modelSVC(X, y)
        elif (self.MlModel == 'DecisionTreeClassifier'):
            return self.modelDecisionTreeClassifier(X, y)
        elif (self.MlModel == 'RandomForestClassifier'):
            return self.modelRandomForestClassifier(X, y)
        elif (self.MlModel == 'GaussianNB'):
            return self.modelGaussianNB(X, y)
        elif (self.MlModel == 'BernoulliNB'):
            return self.modelBernoulliNB(X, y)
        elif (self.MlModel == 'KNeighborsClassifier'):
            return self.modelKNeighborsClassifier(X, y)
        elif (self.MlModel == 'NeuralNetwork'):
            return self.modelNeuralNetwork()
        else:
            warnings.warn(
                "The chosen ML model doesn't exist, wrong input model name!")
            sys.exit(0)

    def modelLogisticRegression(self, X, y):
        model = LogisticRegression(
            penalty=self.lr_penalty,
            dual=self.lr_dual,
            tol=self.lr_tol,
            C=self.lr_C,
            fit_intercept=self.lr_fit_intercept,
            intercept_scaling=self.lr_intercept_scaling,
            class_weight=self.lr_class_weight,
            random_state=self.lr_random_state,
            solver=self.lr_solver,
            max_iter=self.lr_max_iter,
            multi_class=self.lr_multi_class,
            verbose=self.lr_verbose,
            warm_start=self.lr_warm_start,
            n_jobs=self.lr_n_jobs)

        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.lr_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelLinearSVC(self, X, y):
        model = svm.LinearSVC(
            penalty=self.lsvc_penalty,
            loss=self.lsvc_loss,
            dual=self.lsvc_dual,
            tol=self.lsvc_tol,
            C=self.lsvc_C,
            multi_class=self.lsvc_multi_class,
            fit_intercept=self.lsvc_fit_intercept,
            intercept_scaling=self.lsvc_intercept_scaling,
            class_weight=self.lsvc_class_weight,
            verbose=self.lsvc_verbose,
            random_state=self.lsvc_random_state,
            max_iter=self.lsvc_max_iter)

        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.lsvc_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelSVC(self, X, y):
        model = svm.SVC(
            C=self.svc_C,
            kernel=self.svc_kernel,
            degree=self.svc_degree,
            gamma=self.svc_gamma,
            coef0=self.svc_coef0,
            shrinking=self.svc_shrinking,
            probability=self.svc_probability,
            tol=self.svc_tol,
            cache_size=self.svc_cache_size,
            class_weight=self.svc_class_weight,
            verbose=self.svc_verbose,
            max_iter=self.svc_max_iter,
            decision_function_shape=self.svc_decision_function_shape,
            random_state=self.svc_random_state)
        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.svc_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelDecisionTreeClassifier(self, X, y):
        model = tree.DecisionTreeClassifier(
            criterion=self.dt_criterion,
            splitter=self.dt_splitter,
            max_depth=self.dt_max_depth,
            min_samples_split=self.dt_min_samples_split,
            min_samples_leaf=self.dt_min_samples_leaf,
            min_weight_fraction_leaf=self.dt_min_weight_fraction_leaf,
            max_features=self.dt_max_features,
            max_leaf_nodes=self.dt_max_leaf_nodes,
            class_weight=self.dt_class_weight,
            random_state=self.dt_random_state,
            min_impurity_decrease=self.dt_min_impurity_decrease,
            min_impurity_split=self.dt_min_impurity_split,
            presort=self.dt_presort)

        if (self.multiLabel == False):
            return model.fit(
                X,
                y,
                sample_weight=self.dt_sample_weight,
                check_input=self.dt_check_input,
                X_idx_sorted=self.dt_X_idx_sorted)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelRandomForestClassifier(self, X, y):
        model = RandomForestClassifier(
            n_estimators=self.rf_n_estimators,
            criterion=self.rf_criterion,
            max_depth=self.rf_max_depth,
            min_samples_split=self.rf_min_samples_split,
            min_samples_leaf=self.rf_min_samples_leaf,
            min_weight_fraction_leaf=self.rf_min_weight_fraction_leaf,
            max_features=self.rf_max_features,
            max_leaf_nodes=self.rf_max_leaf_nodes,
            min_impurity_decrease=self.rf_min_impurity_decrease,
            min_impurity_split=self.rf_min_impurity_split,
            bootstrap=self.rf_bootstrap,
            oob_score=self.rf_oob_score,
            n_jobs=self.rf_n_jobs,
            random_state=self.rf_random_state,
            verbose=self.rf_verbose,
            warm_start=self.rf_warm_start,
            class_weight=self.rf_class_weight)

        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.rf_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelGaussianNB(self, X, y):
        model = GaussianNB(
            priors=self.gnb_priors, var_smoothing=self.gnb_var_smoothing)
        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.gnb_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelBernoulliNB(self, X, y):
        model = BernoulliNB(
            alpha=self.bnb_alpha,
            binarize=self.bnb_binarize,
            fit_prior=self.bnb_fit_prior,
            class_prior=self.bnb_class_prior)
        if (self.multiLabel == False):
            return model.fit(X, y, sample_weight=self.bnb_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    def modelKNeighborsClassifier(self, X, y):
        model = neighbors.KNeighborsClassifier(
            n_neighbors=self.knn_n_neighbors,
            weights=self.knn_weights,
            algorithm=self.knn_algorithm,
            leaf_size=self.knn_leaf_size,
            p=self.knn_p,
            metric=self.knn_metric,
            metric_params=self.knn_metric_params,
            n_jobs=self.knn_n_jobs)

        if (self.multiLabel == False):
            return model.fit(X, y)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.fit(X, y)

    # check multi-label later
    def modelNeuralNetwork(self):
        model = keras.Sequential()
        return model


def Cal_ROC_curve_parameters(classifier,
                             classifierLabel,
                             x_input,
                             y_true,
                             multiFlag=False,
                             targetIndexInPredict=1):

    if (classifierLabel == 'LogisticRegression') or (
            classifierLabel == 'LinearSVC') or (classifierLabel == 'SVC') or (
                classifierLabel == 'SGD_logistic') or (
                    classifierLabel == 'SGD_linearSVM') or (
                        classifierLabel == 'Passive_Aggressive') or (
                            classifierLabel == 'Perceptron'):
        y_score = classifier.decision_function(x_input)
    elif (classifierLabel == 'DecisionTreeClassifier') or classifierLabel == (
            'RandomForestClassifier') or (classifierLabel == 'GaussianNB') or (
                classifierLabel == 'KNeighborsClassifier') or (
                    classifierLabel == 'BernoulliNB'):
        if (not multiFlag):
            y_score = classifier.predict_proba(
                x_input)[:, targetIndexInPredict]
        else:
            y_score = classifier.predict_proba(x_input)
    else:
        warnings.warn('Wrong chose, no such over classifier label!')
        sys.exit(0)

    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    if (not multiFlag):
        posLabel = classifier.classes_[targetIndexInPredict]
        # Compute average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(
            y_true, y_score, pos_label=posLabel)
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    else:

        # Compute average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(y_true.ravel(),
                                                  y_score.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    return fpr["micro"], tpr["micro"], roc_auc["micro"]


def Cal_PR_curve_parameters(classifier,
                            classifierLabel,
                            x_input,
                            y_true,
                            multiFlag=False,
                            targetIndexInPredict=0):

    if (classifierLabel == 'LogisticRegression') or (
            classifierLabel == 'LinearSVC') or (classifierLabel == 'SVC') or (
                classifierLabel == 'SGD_logistic') or (
                    classifierLabel == 'SGD_linearSVM') or (
                        classifierLabel == 'Passive_Aggressive') or (
                            classifierLabel == 'Perceptron'):
        y_score = classifier.decision_function(x_input)
    elif (classifierLabel == 'DecisionTreeClassifier') or classifierLabel == (
            'RandomForestClassifier') or (classifierLabel == 'GaussianNB') or (
                classifierLabel == 'KNeighborsClassifier') or (
                    classifierLabel == 'BernoulliNB'):
        if (not multiFlag):
            y_score = classifier.predict_proba(
                x_input)[:, targetIndexInPredict]
        else:
            y_score = classifier.predict_proba(x_input)
    else:
        warnings.warn('Wrong chose, no such over classifier label!')
        sys.exit(0)

    y_pred = classifier.predict(x_input)

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    f1Score = dict()

    if (not multiFlag):
        posLabel = classifier.classes_[targetIndexInPredict]
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_true, y_score, pos_label=posLabel)
        average_precision["micro"] = average_precision_score(
            y_true, y_score, pos_label=posLabel)
        f1Score["micro"] = f1_score(y_true, y_pred, pos_label=posLabel)
    else:

        # A "micro-average": quantifying score on all classes jointly
        precision["micro"], recall["micro"], _ = precision_recall_curve(
            y_true.ravel(), y_score.ravel())
        average_precision["micro"] = average_precision_score(
            y_true, y_score, average="micro")
        f1Score["micro"] = f1_score(y_true, y_pred, average='micro')

    return precision["micro"], recall["micro"], average_precision[
        "micro"], f1Score["micro"]


def Cal_nn_ROC_parameters(classifier, x_input, y_true, multiFlag=False):
    '''
        Notice: binary classification or multi classification

        y_true must be binarized.
    '''
    if (not multiFlag):
        predictions = classifier.predict(x_input)[:, 1]
    else:
        predictions = classifier.predict(x_input)

    # print(y_true)
    # print(predictions)

    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    # Compute average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(y_true.ravel(),
                                              predictions.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])

    return fpr["micro"], tpr["micro"], roc_auc["micro"]


def Cal_nn_PR_parameters(classifier, x_input, y_true, labels, multiFlag=False):
    '''
        Notice: binary classification or multi classification

        y_true must be binarized.
    '''

    if (not multiFlag):
        predictions = classifier.predict(x_input)[:, 1]
    else:
        predictions = classifier.predict(x_input)

    # For each class
    precision = dict()
    recall = dict()
    average_precision = dict()
    f1Score = dict()

    # A "micro-average": quantifying score on all classes jointly
    precision["micro"], recall["micro"], _ = precision_recall_curve(
        y_true.ravel(), predictions.ravel())
    average_precision["micro"] = average_precision_score(
        y_true, predictions, average="micro")

    y_pred = list()
    for i in range(len(predictions)):
        y_pred.append(np.argmax(predictions[i]))

    # Use label_binarize to be multi-label like settings
    y_pred_binarized = label_binarize(y_pred, classes=labels)

    # print(y_true)
    # print(y_pred_binarized)

    f1Score["micro"] = f1_score(y_true, y_pred_binarized, average='micro')

    return precision["micro"], recall["micro"], average_precision[
        "micro"], f1Score["micro"]


class onlineClassifierAndItsParameters():
    '''
    '''

    def __init__(self,
                 onlineModel='SGDClassifier',
                 multiLabel=False,
                 multi_n_jobs=None,
                 sgd_loss='hinge',
                 sgd_penalty='l2',
                 sgd_alpha=0.0001,
                 sgd_l1_ratio=0.15,
                 sgd_fit_intercept=True,
                 sgd_max_iter=None,
                 sgd_tol=None,
                 sgd_shuffle=False,
                 sgd_verbose=0,
                 sgd_epsilon=0.1,
                 sgd_n_jobs=1,
                 sgd_random_state=None,
                 sgd_learning_rate='optimal',
                 sgd_eta0=0.0,
                 sgd_power_t=0.5,
                 sgd_early_stopping=False,
                 sgd_validation_fraction=0.1,
                 sgd_n_iter_no_change=5,
                 sgd_class_weight=None,
                 sgd_warm_start=False,
                 sgd_average=False,
                 sgd_n_iter=None,
                 sgd_classes=None,
                 sgd_sample_weight=None,
                 perc_penalty=None,
                 perc_alpha=0.0001,
                 perc_fit_intercept=True,
                 perc_n_iter=5,
                 perc_shuffle=False,
                 perc_verbose=0,
                 perc_eta0=1.0,
                 perc_n_jobs=1,
                 perc_random_state=0,
                 perc_class_weight=None,
                 perc_warm_start=False,
                 perc_classes=None,
                 perc_sample_weight=None,
                 PA_C=1.0,
                 PA_fit_intercept=True,
                 PA_max_iter=None,
                 PA_tol=None,
                 PA_early_stopping=False,
                 PA_validation_fraction=0.1,
                 PA_n_iter_no_change=5,
                 PA_shuffle=True,
                 PA_verbose=0,
                 PA_loss='hinge',
                 PA_n_jobs=None,
                 PA_random_state=None,
                 PA_warm_start=False,
                 PA_class_weight=None,
                 PA_average=False,
                 PA_n_iter=None,
                 PA_classes=None,
                 MNB_alpha=1.0,
                 MNB_fit_prior=True,
                 MNB_class_prior=None,
                 MNB_classes=None,
                 MNB_sample_weight=None,
                 BNB_alpha=1.0,
                 BNB_binarize=0.0,
                 BNB_fit_prior=True,
                 BNB_class_prior=None,
                 BNB_classes=None,
                 BNB_sample_weight=None):
        '''
            According parameter 'onlineModel' selecting online classification model
        '''
        self.onlineModel = onlineModel
        '''
            According parameter 'multiLabel' selecting normal label or multi-label classification, 
            'multi_n_jobs' is the unique additional parameter in multiclass(OneVsRestClassifier). 
        '''
        self.multiLabel = multiLabel
        self.multi_n_jobs = multi_n_jobs

        if self.onlineModel == 'SGDClassifier':
            '''
                control the SGDClassifier model parameters
            '''
            self.sgd_loss = sgd_loss
            self.sgd_penalty = sgd_penalty
            self.sgd_alpha = sgd_alpha
            self.sgd_l1_ratio = sgd_l1_ratio
            self.sgd_fit_intercept = sgd_fit_intercept
            self.sgd_max_iter = sgd_max_iter
            self.sgd_tol = sgd_max_iter
            self.sgd_shuffle = sgd_shuffle
            self.sgd_verbose = sgd_verbose
            self.sgd_epsilon = sgd_epsilon
            self.sgd_n_jobs = sgd_n_jobs
            self.sgd_random_state = sgd_random_state
            self.sgd_learning_rate = sgd_learning_rate
            self.sgd_eta0 = sgd_eta0
            self.sgd_power_t = sgd_power_t
            self.sgd_early_stopping = sgd_early_stopping
            self.sgd_validation_fraction = sgd_validation_fraction
            self.sgd_n_iter_no_change = sgd_n_iter_no_change
            self.sgd_class_weight = sgd_class_weight
            self.sgd_warm_start = sgd_warm_start
            self.sgd_average = sgd_average
            self.sgd_n_iter = sgd_n_iter
            '''
                control the parameters of partial_fit function of SGDClassifier
            '''
            self.sgd_classes = sgd_classes
            self.sgd_sample_weight = sgd_sample_weight
        elif self.onlineModel == 'Perceptron':
            '''
                control the Perceptron model parameters
            '''
            self.perc_penalty = perc_penalty
            self.perc_alpha = perc_alpha
            self.perc_fit_intercept = perc_fit_intercept
            self.perc_n_iter = perc_n_iter
            self.perc_shuffle = perc_shuffle
            self.perc_verbose = perc_verbose
            self.perc_eta0 = perc_eta0
            self.perc_n_jobs = perc_n_jobs
            self.perc_random_state = perc_random_state
            self.perc_class_weight = perc_class_weight
            self.perc_warm_start = perc_warm_start
            '''
                control the parameters of partial_fit function of Perceptron
            '''
            self.perc_classes = perc_classes
            self.perc_sample_weight = perc_sample_weight
        elif self.onlineModel == 'PassiveAggressiveClassifier':
            '''
                control the PassiveAggressiveClassifier model parameters
            '''
            self.PA_C = PA_C
            self.PA_fit_intercept = PA_fit_intercept
            self.PA_max_iter = PA_max_iter
            self.PA_tol = PA_tol
            self.PA_early_stopping = PA_early_stopping
            self.PA_validation_fraction = PA_validation_fraction
            self.PA_n_iter_no_change = PA_n_iter_no_change
            self.PA_shuffle = PA_shuffle
            self.PA_verbose = PA_verbose
            self.PA_loss = PA_loss
            self.PA_n_jobs = PA_n_jobs
            self.PA_random_state = PA_random_state
            self.PA_warm_start = PA_warm_start
            self.PA_class_weight = PA_class_weight
            self.PA_average = PA_average
            self.PA_n_iter = PA_n_iter
            '''
                control the parameters of partial_fit function of PassiveAggressiveClassifier
            '''
            self.PA_classes = PA_classes
        elif self.onlineModel == 'MultinomialNB':
            '''
                control the MultinomialNB model parameters
            '''
            self.MNB_alpha = MNB_alpha
            self.MNB_fit_prior = MNB_fit_prior
            self.MNB_class_prior = MNB_class_prior
            '''
                control the parameters of partial_fit function of MultinomialNB
            '''
            self.MNB_classes = MNB_classes
            self.MNB_sample_weight = MNB_sample_weight
        elif self.onlineModel == 'BernoulliNB':
            '''
                control the BernoulliNB model parameters
            '''
            self.BNB_alpha = BNB_alpha
            self.BNB_binarize = BNB_binarize
            self.BNB_fit_prior = BNB_fit_prior
            self.BNB_class_prior = BNB_class_prior
            '''
                control the parameters of partial_fit function of BernoulliNB
            '''
            self.BNB_classes = BNB_classes
            self.BNB_sample_weight = BNB_sample_weight
        else:
            warnings.warn(
                "The chosen online model doesn't exist, check if it's wrong model name!"
            )
            exit()

    def onlineModelAndFittingTrainSet(self, X, y):

        if (self.onlineModel == 'SGDClassifier'):
            return self.onlineModelSGDClassifier(X, y)
        elif (self.onlineModel == 'Perceptron'):
            return self.onlineModelPerceptron(X, y)
        elif (self.onlineModel == 'PassiveAggressiveClassifier'):
            return self.onlineModelPassiveAggressiveClassifier(X, y)
        elif (self.onlineModel == 'MultinomialNB'):
            return self.onlineModelMultinomialNB(X, y)
        elif (self.onlineModel == 'BernoulliNB'):
            return self.onlineModelBernoulliNB(X, y)
        else:
            warnings.warn(
                "The chosen online model doesn't exist, wrong input model name!"
            )
            sys.exit(0)

    def onlineModelSGDClassifier(self, X, y):
        model = SGDClassifier(
            loss=self.sgd_loss,
            penalty=self.sgd_penalty,
            alpha=self.sgd_alpha,
            l1_ratio=self.sgd_l1_ratio,
            fit_intercept=self.sgd_fit_intercept,
            max_iter=self.sgd_max_iter,
            tol=self.sgd_max_iter,
            shuffle=False,
            verbose=self.sgd_verbose,
            epsilon=self.sgd_epsilon,
            n_jobs=self.sgd_n_jobs,
            random_state=self.sgd_random_state,
            learning_rate=self.sgd_learning_rate,
            eta0=self.sgd_eta0,
            power_t=self.sgd_power_t,
            early_stopping=self.sgd_early_stopping,
            validation_fraction=self.sgd_validation_fraction,
            n_iter_no_change=self.sgd_n_iter_no_change,
            class_weight=self.sgd_class_weight,
            warm_start=self.sgd_warm_start,
            average=self.sgd_average,
            n_iter=self.sgd_n_iter)

        if (self.multiLabel == False):
            return model.partial_fit(
                X,
                y,
                classes=self.sgd_classes,
                sample_weight=self.sgd_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.partial_fit(X, y, classes=self.sgd_classes)

    def onlineModelPerceptron(self, X, y):
        model = Perceptron(
            penalty=self.perc_penalty,
            alpha=self.perc_alpha,
            fit_intercept=self.perc_fit_intercept,
            n_iter=self.perc_n_iter,
            shuffle=self.perc_shuffle,
            verbose=self.perc_verbose,
            eta0=self.perc_eta0,
            n_jobs=self.perc_n_jobs,
            random_state=self.perc_random_state,
            class_weight=self.perc_class_weight,
            warm_start=self.perc_warm_start)

        if (self.multiLabel == False):
            return model.partial_fit(
                X,
                y,
                classes=self.perc_classes,
                sample_weight=self.perc_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.partial_fit(
                X, y, classes=self.perc_classes)

    def onlineModelPassiveAggressiveClassifier(self, X, y):
        model = PassiveAggressiveClassifier(
            C=self.PA_C,
            fit_intercept=self.PA_fit_intercept,
            max_iter=self.PA_max_iter,
            tol=self.PA_tol,
            early_stopping=self.PA_early_stopping,
            validation_fraction=self.PA_validation_fraction,
            n_iter_no_change=self.PA_n_iter_no_change,
            shuffle=self.PA_shuffle,
            verbose=self.PA_verbose,
            loss=self.PA_loss,
            n_jobs=self.PA_n_jobs,
            random_state=self.PA_random_state,
            warm_start=self.PA_warm_start,
            class_weight=self.PA_class_weight,
            average=self.PA_average,
            n_iter=self.PA_n_iter)

        if (self.multiLabel == False):
            return model.partial_fit(X, y, classes=self.PA_classes)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.partial_fit(X, y, classes=self.PA_classes)

    def onlineModelMultinomialNB(self, X, y):
        model = MultinomialNB(
            alpha=self.MNB_alpha,
            fit_prior=self.MNB_fit_prior,
            class_prior=self.MNB_class_prior)

        if (self.multiLabel == False):
            return model.partial_fit(
                X,
                y,
                classes=self.MNB_classes,
                sample_weight=self.MNB_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.partial_fit(X, y, classes=self.MNB_classes)

    def onlineModelBernoulliNB(self, X, y):
        model = BernoulliNB(
            alpha=self.BNB_alpha,
            binarize=self.BNB_binarize,
            fit_prior=self.BNB_fit_prior,
            class_prior=self.BNB_class_prior)

        if (self.multiLabel == False):
            return model.partial_fit(
                X,
                y,
                classes=self.BNB_classes,
                sample_weight=self.BNB_sample_weight)
        else:
            model_multiLabel = OneVsRestClassifier(
                model, n_jobs=self.multi_n_jobs)
            return model_multiLabel.partial_fit(X, y, classes=self.BNB_classes)


def getByteInfoArray(pktsData, featuresOfOneByte, fixedColumn=False):

    if not fixedColumn:
        '''
        'featuresOfOneByte' is 10, 8 or 6.

        10 features of one byte:
            feature 0: byte position;
            feature 1: byte value;
            feature 2-9: byte around the current byte(triangle)

        8 features of one byte:
            feature 0: byte position;
            feature 1: byte value;
            feature 2-7: byte around(left over right, 2 bytes)

        6 features of one byte:
            feature 0: byte position;
            feature 1: byte value;
            feature 2-5: byte around(over 2, left 1, right 1)
        '''
        if featuresOfOneByte == 10:

            byteInfoArray = np.zeros(
                shape=(len(pktsData) * pktsData.shape[1], featuresOfOneByte))
            for rowIndex in range(len(pktsData)):
                for columnIndex in range(pktsData.shape[1]):
                    if rowIndex == 0:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0, 0,
                                    0, 0, 0,
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0,
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    0, pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == pktsData.shape[1] - 2:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2], 0,
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    0, pktsData[rowIndex, columnIndex + 1], 0
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2], 0,
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    0, 0, 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2], 0,
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    0, pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                    elif rowIndex == 1:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0, 0,
                                    0, pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0,
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == pktsData.shape[1] - 2:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1], 0
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex], 0, 0,
                                    0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                    else:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0, 0,
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0,
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == pktsData.shape[1] - 2:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1], 0
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex], 0, 0,
                                    0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex - 1, columnIndex - 1],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
            return byteInfoArray

        elif featuresOfOneByte == 8:

            byteInfoArray = np.zeros(
                shape=(len(pktsData) * pktsData.shape[1], featuresOfOneByte))
            for rowIndex in range(len(pktsData)):
                for columnIndex in range(pktsData.shape[1]):
                    if rowIndex == 0:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0, 0,
                                    0, pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0,
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == pktsData.shape[1] - 2:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    pktsData[rowIndex, columnIndex + 1], 0
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    0, 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                    elif rowIndex == 1:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0, 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0,
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == pktsData.shape[1] - 2:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1], 0
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex], 0, 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                    else:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0,
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0,
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
                        elif columnIndex == pktsData.shape[1] - 2:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1], 0
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex], 0, 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 2],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1],
                                    pktsData[rowIndex, columnIndex + 2]
                                ]
            return byteInfoArray

        elif featuresOfOneByte == 6:

            byteInfoArray = np.zeros(
                shape=(len(pktsData) * pktsData.shape[1], featuresOfOneByte))
            for rowIndex in range(len(pktsData)):
                for columnIndex in range(pktsData.shape[1]):
                    if rowIndex == 0:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0, 0,
                                    pktsData[rowIndex, columnIndex + 1]
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 1], 0, 0,
                                    pktsData[rowIndex, columnIndex + 1]
                                ]
                    elif rowIndex == 1:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0, 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1]
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex], 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 1], 0,
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1]
                                ]

                    else:
                        if columnIndex == 0:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex], 0,
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1]
                                ]
                        elif columnIndex == pktsData.shape[1] - 1:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex], 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex, columnIndex - 1],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex, columnIndex + 1]
                                ]
            return byteInfoArray

        else:
            warnings.warn("wrong dimentions of 'featuresOfOneByte'!")
            sys.exit(0)
    else:
        if featuresOfOneByte == 6:
            byteInfoArray = np.zeros(
                shape=(len(pktsData) * pktsData.shape[1],
                       featuresOfOneByte + 1))
            for rowIndex in range(len(pktsData)):
                for columnIndex in range(pktsData.shape[1]):
                    if rowIndex == 0:
                        if pktsData[rowIndex, columnIndex] == 0:
                            byteInfoArray[rowIndex * pktsData.shape[1] +
                                          columnIndex] = [
                                              columnIndex,
                                              pktsData[rowIndex, columnIndex],
                                              0, 0, 0, 0, 0
                                          ]
                        else:
                            byteInfoArray[rowIndex * pktsData.shape[1] +
                                          columnIndex] = [
                                              columnIndex,
                                              pktsData[rowIndex, columnIndex],
                                              0, 0, 0, 0, 1
                                          ]
                    elif rowIndex == 1:
                        if pktsData[rowIndex, columnIndex] == pktsData[
                                rowIndex - 1, columnIndex]:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex], 0, 0,
                                    0, 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex], 0, 0,
                                    0, 1
                                ]
                    elif rowIndex == 2:
                        if (pktsData[rowIndex, columnIndex] ==
                                pktsData[rowIndex - 1, columnIndex]) and (
                                    pktsData[rowIndex - 1, columnIndex] ==
                                    pktsData[rowIndex - 2, columnIndex]):
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 2, columnIndex], 0, 0,
                                    0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 2, columnIndex], 0, 0,
                                    1
                                ]
                    elif rowIndex == 3:
                        if (pktsData[rowIndex, columnIndex] ==
                                pktsData[rowIndex - 1, columnIndex]) and (
                                    pktsData[rowIndex - 1, columnIndex] ==
                                    pktsData[rowIndex - 2, columnIndex]) and (
                                        pktsData[rowIndex - 2, columnIndex] ==
                                        pktsData[rowIndex - 3, columnIndex]):
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 3, columnIndex], 0, 0
                                ]
                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 3, columnIndex], 0, 1
                                ]
                    else:
                        if (pktsData[rowIndex, columnIndex] ==
                                pktsData[rowIndex - 1, columnIndex]
                            ) and (
                                pktsData[rowIndex - 1, columnIndex] ==
                                pktsData[rowIndex - 2, columnIndex]) and (
                                    pktsData[rowIndex - 2, columnIndex] ==
                                    pktsData[rowIndex - 3, columnIndex]) and (
                                        pktsData[rowIndex - 3, columnIndex] ==
                                        pktsData[rowIndex - 4, columnIndex]):
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 3, columnIndex],
                                    pktsData[rowIndex - 4, columnIndex], 0
                                ]

                        else:
                            byteInfoArray[
                                rowIndex * pktsData.shape[1] + columnIndex] = [
                                    columnIndex,
                                    pktsData[rowIndex, columnIndex],
                                    pktsData[rowIndex - 1, columnIndex],
                                    pktsData[rowIndex - 2, columnIndex],
                                    pktsData[rowIndex - 3, columnIndex],
                                    pktsData[rowIndex - 4, columnIndex], 1
                                ]

            #########################################################################
            # byteInfoArray = np.zeros(
            #     shape=(len(pktsData) * pktsData.shape[1],
            #            featuresOfOneByte + 4))
            # for rowIndex in range(len(pktsData)):
            #     for columnIndex in range(pktsData.shape[1]):
            #         if rowIndex == 0:
            #             if pktsData[rowIndex, columnIndex] == 0:
            #                 byteInfoArray[rowIndex * pktsData.shape[1] +
            #                               columnIndex] = [
            #                                   columnIndex,
            #                                   pktsData[rowIndex, columnIndex],
            #                                   0, 0, 0, 0, 1, 0, 0, 0
            #                               ]
            #             else:
            #                 byteInfoArray[rowIndex * pktsData.shape[1] +
            #                               columnIndex] = [
            #                                   columnIndex,
            #                                   pktsData[rowIndex, columnIndex],
            #                                   0, 0, 0, 0, 0, 0, 0, 1
            #                               ]
            #         elif rowIndex == 1:
            #             if pktsData[rowIndex, columnIndex] == pktsData[
            #                     rowIndex - 1, columnIndex]:
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex], 0, 0,
            #                         0, 1, 0, 0, 0
            #                     ]
            #             else:
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex], 0, 0,
            #                         0, 0, 0, 0, 1
            #                     ]
            #         elif rowIndex == 2:
            #             if (pktsData[rowIndex, columnIndex] ==
            #                     pktsData[rowIndex - 1, columnIndex]) and (
            #                         pktsData[rowIndex - 1, columnIndex] ==
            #                         pktsData[rowIndex - 2, columnIndex]):
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex], 0, 0,
            #                         1, 0, 0, 0
            #                     ]
            #             else:
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex], 0, 0,
            #                         0, 0, 0, 1
            #                     ]
            #         elif rowIndex == 3:
            #             if (pktsData[rowIndex, columnIndex] ==
            #                     pktsData[rowIndex - 1, columnIndex]) and (
            #                         pktsData[rowIndex - 1, columnIndex] ==
            #                         pktsData[rowIndex - 2, columnIndex]) and (
            #                             pktsData[rowIndex - 2, columnIndex] ==
            #                             pktsData[rowIndex - 3, columnIndex]):
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex],
            #                         pktsData[rowIndex - 3, columnIndex], 0, 1,
            #                         0, 0, 0
            #                     ]
            #             else:
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex],
            #                         pktsData[rowIndex - 3, columnIndex], 0, 0,
            #                         0, 0, 1
            #                     ]
            #         else:
            #             if (pktsData[rowIndex, columnIndex] ==
            #                     pktsData[rowIndex - 1, columnIndex]
            #                 ) and (
            #                     pktsData[rowIndex - 1, columnIndex] ==
            #                     pktsData[rowIndex - 2, columnIndex]) and (
            #                         pktsData[rowIndex - 2, columnIndex] ==
            #                         pktsData[rowIndex - 3, columnIndex]) and (
            #                             pktsData[rowIndex - 3, columnIndex] ==
            #                             pktsData[rowIndex - 4, columnIndex]):
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex],
            #                         pktsData[rowIndex - 3, columnIndex],
            #                         pktsData[rowIndex - 4, columnIndex], 1, 0,
            #                         0, 0
            #                     ]
            #             elif (pktsData[rowIndex, columnIndex] >=
            #                   pktsData[rowIndex - 1, columnIndex]) and (
            #                       pktsData[rowIndex - 1, columnIndex] >=
            #                       pktsData[rowIndex - 2, columnIndex]
            #                   ) and (pktsData[rowIndex - 2, columnIndex] >=
            #                          pktsData[rowIndex - 3, columnIndex]) and (
            #                              pktsData[rowIndex - 3, columnIndex] >=
            #                              pktsData[rowIndex - 4, columnIndex]):
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex],
            #                         pktsData[rowIndex - 3, columnIndex],
            #                         pktsData[rowIndex - 4, columnIndex], 0, 1,
            #                         0, 0
            #                     ]
            #             elif (pktsData[rowIndex, columnIndex] <=
            #                   pktsData[rowIndex - 1, columnIndex]) and (
            #                       pktsData[rowIndex - 1, columnIndex] <=
            #                       pktsData[rowIndex - 2, columnIndex]
            #                   ) and (pktsData[rowIndex - 2, columnIndex] <=
            #                          pktsData[rowIndex - 3, columnIndex]) and (
            #                              pktsData[rowIndex - 3, columnIndex] <=
            #                              pktsData[rowIndex - 4, columnIndex]):
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex],
            #                         pktsData[rowIndex - 3, columnIndex],
            #                         pktsData[rowIndex - 4, columnIndex], 0, 0,
            #                         1, 0
            #                     ]
            #             else:
            #                 byteInfoArray[
            #                     rowIndex * pktsData.shape[1] + columnIndex] = [
            #                         columnIndex,
            #                         pktsData[rowIndex, columnIndex],
            #                         pktsData[rowIndex - 1, columnIndex],
            #                         pktsData[rowIndex - 2, columnIndex],
            #                         pktsData[rowIndex - 3, columnIndex],
            #                         pktsData[rowIndex - 4, columnIndex], 0, 0,
            #                         0, 1
            #                     ]
            return byteInfoArray
        else:
            warnings.warn("wrong dimentions of 'featuresOfOneByte'!")
            sys.exit(0)


def iter_miniBytesBatches(pktsInfo, bytesInfo, bytesLabel, labelBinarized_flag,
                          devideNum):

    pktsSets = np.array_split(pktsInfo, devideNum, axis=0)
    miniBytesInfo = list()
    miniBytesLabel = list()

    firstPosi = 0
    for i, item in enumerate(pktsSets):
        miniBytesInfo.append(
            bytesInfo[firstPosi:firstPosi +
                      len(pktsSets[i]) * pktsInfo.shape[1], :])
        if not labelBinarized_flag:
            miniBytesLabel.append(
                bytesLabel[firstPosi:firstPosi +
                           len(pktsSets[i]) * pktsInfo.shape[1]])
        else:
            miniBytesLabel.append(
                bytesLabel[firstPosi:firstPosi +
                           len(pktsSets[i]) * pktsInfo.shape[1], :])
        firstPosi += len(pktsSets[i]) * pktsInfo.shape[1]

    return miniBytesInfo, miniBytesLabel


def trainByteSetIncreasing(pktsInfo, bytesInfo, bytesLabel, initial_size,
                           step_size, end_size, labelBinarized_flag):
    '''
        initial_size < end_size <=len(pktsInfo)
    '''

    if (len(pktsInfo) >= end_size):

        pktsSets = list()
        temp_size = initial_size
        while temp_size <= end_size:
            pktsSets.append(pktsInfo[0:temp_size, :])
            temp_size += step_size

    else:
        warnings.warn(
            "wrong size of 'end_size', should be less than len(pktsInfo)")
        print('pkts size: ', len(pktsInfo))

        pktsSets = list()
        temp_size = initial_size

        while temp_size <= len(pktsInfo):
            pktsSets.append(pktsInfo[0:temp_size, :])
            temp_size += step_size
        else:
            pktsSets.append(pktsInfo[0:len(pktsInfo), :])

    miniBytesInfo = list()
    miniBytesLabel = list()

    for i, item in enumerate(pktsSets):
        miniBytesInfo.append(
            bytesInfo[0:len(pktsSets[i]) * pktsInfo.shape[1], :])
        if not labelBinarized_flag:
            miniBytesLabel.append(
                bytesLabel[0:len(pktsSets[i]) * pktsInfo.shape[1]])
        else:
            miniBytesLabel.append(
                bytesLabel[0:len(pktsSets[i]) * pktsInfo.shape[1], :])

    return miniBytesInfo, miniBytesLabel


def trainPktSetWithoutLabelsInput(pktsInfo, initial_size, step_size, end_size):
    '''
        initial_size < end_size <=len(pktsInfo)
    '''

    if (len(pktsInfo) >= end_size):

        pktsSets = list()
        temp_size = initial_size
        while temp_size <= end_size:
            pktsSets.append(pktsInfo[0:temp_size, :])
            temp_size += step_size

    else:
        warnings.warn(
            "wrong size of 'end_size', should be less than len(pktsInfo)")
        print('pkts size: ', len(pktsInfo))

        pktsSets = list()
        temp_size = initial_size

        while temp_size <= len(pktsInfo):
            pktsSets.append(pktsInfo[0:temp_size, :])
            temp_size += step_size
        else:
            pktsSets.append(pktsInfo[0:len(pktsInfo), :])

    return pktsSets


def mean_confidence_interval(data, confidence=0.95):
    a = 1.0 * np.array(data)
    n = len(a)
    m, se = np.mean(a), scipy.stats.sem(a)
    h = se * scipy.stats.t.ppf((1 + confidence) / 2., n - 1)
    return m, h, h