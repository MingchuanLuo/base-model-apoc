"""
======================
Read and write graphs.
======================

Read and write graphs.
"""

import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
from numpy.core.fromnumeric import size

nodes=['Deplaning','Unloading', 'Catering','Cleaning', 'Fuelling', 'Loading', 'Boarding']
nodes2=['(21087)','(7682)','(9280)','(16161)','(15310)','(17467)','(20815)']
edges=[('Deplaning','Unloading',7562),('Deplaning','Catering',9056),('Deplaning','Cleaning',15816),('Deplaning','Fuelling',14926),('Deplaning','Loading',16629),('Deplaning','Boarding',20135),
        ('Unloading','Catering',2218),('Unloading','Cleaning',5565),('Unloading','Fuelling',5441),('Unloading','Loading',6293),('Unloading','Boarding',7310),
        ('Catering','Cleaning',8157),('Catering','Fuelling',8218),('Catering','Loading',7254),('Catering','Boarding',8948),
        ('Cleaning','Fuelling',12286),('Cleaning','Loading',12997),('Cleaning','Boarding',15522),
        ('Fuelling','Loading',12245),('Fuelling','Boarding',14761),
        ('Loading','Boarding',16474)]

pos = {'Deplaning': [0.13,0.1],'Unloading': [0,0.5],'Catering': [0.45,.75],'Cleaning': [1.05,0.75],'Fuelling': [1.5,0.5],'Loading': [1.37,0.1],'Boarding': [0.75, -0.15]}
pos1 ={'Deplaning':[0.13,0.12],'Unloading':[0,0.52],'Catering':[0.45,.77],'Cleaning': [1.05,0.77], 'Fuelling':[1.5,0.52],'Loading':[1.37,0.12],'Boarding':[0.75, -0.13]}
pos2 ={'(21087)':[0.13,0.08],'(7682)':[0,0.48],'(9280)':[0.45,.73],'(16161)':[1.05,0.73], '(15310)':[1.5,0.48],'(17467)':[1.37,0.08],'(20815)':[0.75, -0.17]}


#定义graph
G = nx.Graph()
H = nx.Graph()
G.add_nodes_from(nodes)
G.add_weighted_edges_from(edges)
H.add_nodes_from(nodes2)

#找到所有连通子图
print('connected_components of graph: ',list(nx.connected_components(G)))

# #显示该graph
# nx.draw(G, with_labels=True, font_weight='bold')
# plt.axis('on')
# plt.xticks([])
# plt.yticks([])
# plt.show()

plt.figure(figsize=[8,8])

totalSubSize1=[21087.0,7682.0,9280.0,16161.0,15310.0,17467.0,20815.0]
totalSubSize = np.array(totalSubSize1) 

nx.draw_networkx_nodes(G, pos, node_size= totalSubSize/4,
node_color=['lightskyblue','steelblue','palegreen','mediumseagreen','lightpink', 'red','sandybrown'])

lineweights = [d["weight"] for (u, v, d) in G.edges(data=True)]

nx.draw_networkx_edges(
    G,
    pos,
    width= np.array(lineweights)/1000,
    alpha=0.4,
)

# labels
nx.draw_networkx_labels(G, pos1, font_size=10, font_family="sans-serif",font_weight='bold')
nx.draw_networkx_labels(H, pos2, font_size=10, font_family="sans-serif") #labels = np.array(['(21087)','(7682)','(9280)','(16161)','(15310)','(17467)','(20815)'])


labels = nx.get_edge_attributes(G,'weight')
nx.draw_networkx_edge_labels(G,pos,edge_labels=labels,font_size=8.5)
#[10,20,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
plt.axis("off")

plt.tight_layout()
plt.xlim([-0.5,2.0])
plt.ylim([-0.5,1.0])
plt.gcf().subplots_adjust(wspace=0)
plt.gcf().subplots_adjust(hspace=0)
plt.gcf().subplots_adjust(left=0.0)
plt.gcf().subplots_adjust(right=1.0)
plt.gcf().subplots_adjust(bottom=0.0)
plt.gcf().subplots_adjust(top=1.0)
#plt.show()
plt.savefig('totalconnecSub1.png', bbox_inches='tight')

