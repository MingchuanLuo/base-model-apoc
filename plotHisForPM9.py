import matplotlib.pyplot as plt
import numpy as np

Result1 = [0.125, 0.151, 0.147, 0.162, 0.221,0.204,0.158]
Result2 = [0.272, 0, 0.358, 0.698, 0.697,0.681,0.635]
Result3 = [0.585, 0.151, 0.562, 0.764, 0.763,0.762,0.729] 

A = ['glr', 'svm', 'dt', 'rf', 'gb','xgb','nn']

x = range(len(Result1))

rechts1=plt.bar(x, height=Result3, width=0.4, alpha=0.8)
plt.ylim(0,0.8)
plt.xticks(x, A)
plt.ylabel("R^2 score", size =11 )
plt.title("Comparation of the 7 regression models with Dataset 3")


plt.show()

# import matplotlib.pyplot as plt
# import matplotlib

# matplotlib.rcParams['font.sans-serif'] = ['SimHei']
# matplotlib.rcParams['axes.unicode_minus'] = False

# label_list = ['2014', '2015', '2016', '2017']
# num_list1 = [20, 30, 15, 35]
# num_list2 = [15, 30, 40, 20]
# x = range(len(num_list1))
# rects1 = plt.bar(x, height=num_list1, width=0.45, alpha=0.8, color='red', label="一部门")
# rects2 = plt.bar(x, height=num_list2, width=0.45, color='green', label="二部门", bottom=num_list1)
# plt.ylim(0, 80)
# plt.ylabel("数量")
# plt.xticks(x, label_list)
# plt.xlabel("年份")
# plt.title("某某公司")
# plt.legend()
# plt.show()

# import pandas as pd
# import numpy as np

# data = pd.DataFrame({'artist_hotness': [0,1,5,np.nan], 'aa': [0,1,5,np.nan]})
# print (data)


# mean_artist_hotness = data['artist_hotness'].mean(skipna=True)
# print (mean_artist_hotness)


# # data=data.replace(0,mean_artist_hotness) 
# # print (data)

# data=data.replace({'artist_hotness': {0: mean_artist_hotness}}) 
# print (data)

# print([[0, 0], [0, 0], [1, 1]])